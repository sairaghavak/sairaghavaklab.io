---
# This is called Front matter
layout: post
title:  "Dev Containers Overview"
date:   2023-06-23 00:42 IST
author: SaiRaghava K
categories: vscode
---

- TODOS: Describe devcontainer lifecycle
- How to do volume mount?
  - Ensured that the vscode-server-extensions are volume mounted so that each time your restart your container, it doesn't have to download and install all extensions rather it will just refer to the linked volume mount folder in docker
  - Also ensured that the local maven repository is also docker volume mounted reasoning is same as the above
- How does the git commits and pushes work from within the container?
  - There would be an ssh-agent on host machine that would forward and enable again inside your docker container
- What's the advantage of this?
  - When somebody clones your repo, they just need a git, docker, and vscode installed in their box and rest is inside the repo. Below are some of things that'll happen when you open the project from remote explorer extension
    - vscode client is running in your host os and vscode-server is running insider the docker container
    - First it will build an image if not already cached
    - Then it will spin up the container and creates volumes if not already created and mounts the volumes i.e., links them to the current container instances
    - Now technically you are inside the container i.e., all your data is inside the container
      - Code - Folder mounted i.e., bind mount - Changes made inside container will be reflected on file system and viceversa
      - vscode-server-extensions - are docker volume mounted - Meaning though your container is stateless, each time its spun it will link and refer to the shared volume which has got data already in it from previous container run
    - Clear separation of things
      - This makes your dev environment separate from project to project and keeps things separate nicely

**References:**

1. [Developing inside the container](https://code.visualstudio.com/docs/devcontainers/containers)
