---
# This is called Front matter
layout: post
title:  "#x VS Code Extensions"
date:   2023-05-04 00:42 IST
author: SaiRaghava K
categories: vscode
---

Here are some of the useful vscode extensions that I use outside or inside dev containers for my development work

1. Eclipse KeyMap
   - To use the same keyboards shortcuts like Eclipse IDE
   - VS Code Extension id: [`alphabotsec.vscode-eclipse-keybindings`](https://marketplace.visualstudio.com/items?itemName=alphabotsec.vscode-eclipse-keybindings)

2. Java Extension Pack
   - Includes {language support for java, debugger for java, test runner for java, maven for java, project manager for java, intellicode}
   - VS Code Extension id: [`vscjava.vscode-java-pack`](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)

3. Run On Save
   - I am using this to perform google java format on save
   - VS Code Extension id: [`emeraldwalk.runonsave`](https://marketplace.visualstudio.com/items?itemName=emeraldwalk.RunOnSave)

4. Spring Boot Extension pack
   - An extension pack to develop spring boot applications
   - VS Code Extension id: [`vmware.vscode-boot-dev-pack`](https://marketplace.visualstudio.com/items?itemName=vmware.vscode-boot-dev-pack)

5. Rest Client
   - Helps to quickly test REST APIs
   - VS Code Extension id: [`humao.rest-client`](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)

6. DB Client
   - Can be used to connect and query to Posgresql, MySQL servers
   - VS Code Extension id: [`cweijan.vscode-mysql-client2`](https://marketplace.visualstudio.com/items?itemName=cweijan.vscode-mysql-client2)

7. XML Language support
   - Provides support for creating and editing xmls
   - VS Code Extension id: [`redhat.vscode-xml`](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml)

8. Dependency Analytics
   - Provides insights about your application dependencies: Security, License compatibility and AI based guidance to choose appropriate dependencies for your application.
   - VS Code Extension id: [`redhat.fabric8-analytics`](https://marketplace.visualstudio.com/items?itemName=redhat.fabric8-analytics)

9. SonarLint
   - While writing code, this extension will auto detect the errors, bad coding practices, vulnerabilitiees with fix suggestions.
   - VS Code Extension id: [`SonarSource.sonarlint-vscode`](https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarlint-vscode)

10. Live Server
    - Useful extension to see the changes live when developing static sites using html,css
    - VS Code Extension id: [`ritwickdey.liveserver`](https://marketplace.visualstudio.com/items?itemName=ritwickdey.liveserver)

11. MarkdownLint
    - Helps to format your markdown files and detects any formatting issues, prompts with fix suggestions.
    - VS Code Extension id: [`davidanson.vscode-markdownlint`](https://marketplace.visualstudio.com/items?itemName=davidanson.vscode-markdownlint)

12. PDF Preview
    - Helps to open and preview a pdf in vscode
    - VS Code Extension id: [`analytic-signal.preview-pdf`](https://marketplace.visualstudio.com/items?itemName=analytic-signal.preview-pdf)

13. Drawio
    - To draw flow charts and Architecture diagrams
    - VS Code Extension id: [`hediet.vscode-drawio`](https://marketplace.visualstudio.com/items?itemName=hediet.vscode-drawio)

14. Plant uml
    - To draw UML diagrams and also some supports some non-uml diagrams as well
    - VS Code Extension id: [`jebbs.plantuml`](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)
    - This requires Java, Graphviz. [PlantUML for the impatient](https://plantuml.com/starting)
    - To verify graphviz installation. Do `dot -V`
      > dot - graphviz version 2.43.0 (0)
    - Note: If your devcontainer is based on the base image: [sairaghavak/java-dev-tookit](https://hub.docker.com/r/sairaghavak/java-dev-toolkit) then your container already has the graphviz installed, because I have installed `graphviz` in the base image itself.

15. Open API(Swagger) Editor
    - To update swagger documentation in `swagger.yaml` files
    - VS Code Extension id: [`42Crunch.vscode-openapi`](https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi)

16. Kotlin Language Server
    - For kotlin linting and support in vscode
    - VS Code Extension id: [`fwcd.kotlin`](https://marketplace.visualstudio.com/items?itemName=fwcd.kotlin)

17. Kotlin code formatter
    - For kotlin code formatting in vscode
    - VS Code Extension id: [`esafirm.kotlin-formatter`](https://marketplace.visualstudio.com/items?itemName=esafirm.kotlin-formatter)

May be having an extension pack would help, I might have to create one.

Summary of the above-mentioned extensions in `devcontainer.json` under extensions section.

```json
"extensions": [
   "alphabotsec.vscode-eclipse-keybindings", // 1. eclipse key bindings
    "vscjava.vscode-java-pack", // 2. Extension pack for java: includes {language support for java, debugger for java, test runner for java, maven for java, project manager for java, intellicode}
    "emeraldwalk.runonsave", // 3. I am using this to perform google java format on save
    "vmware.vscode-boot-dev-pack", // 4. includes spring boot tools, initializer, dashboard extensions
    "humao.rest-client", // 5. To quickly check test and check APIs, alternative to POSTMAN
    "cweijan.vscode-mysql-client2", // 6. DB client
    "redhat.vscode-xml", // 7. XML Language support
    "redhat.fabric8-analytics", // 8. Dependency analytics
    "sonarsource.sonarlint-vscode", // 9. Sonarlint
    "ritwickdey.liveserver", // 10. Live server that will auto refersh upon changes to static pages
    "davidanson.vscode-markdownlint", // 11. Markdownlint
    "analytic-signal.preview-pdf", // 12. Preview pdfs
    "hediet.vscode-drawio", // 13. For drawing architecture diagrams
    "jebbs.plantuml", // 14. Plantuml for uml and non-uml diagrams
    "42Crunch.vscode-openapi", // 15. For live preview of OAS/swagger spec in vscode
    "fwcd.kotlin", // 16. kotlin language server
    "esafirm.kotlin-formatter", // 17. kotlin formatter
  ]
```

- **Recommendation:** When using vscode devcontainer setup, apply/enable/install only specific extensions. Example, while working in GoLang devcontainer setup, enable only extensions specific to GoLang and some language agnostic exntensions to keep the container light and free from unrelated extensions.

**Note:** I will keep updating this post based on the extensions that are essential.
