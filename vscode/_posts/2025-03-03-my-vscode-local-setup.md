---
# This is called Front matter
layout: post
title:  "My VS Code Setup & Settings Sync workflow"
date:   2025-03-03 05:36 IST
author: SaiRaghava K
categories: vscode
comments: true
---

Preread: [VsCode Multi-root Workspaces](https://code.visualstudio.com/docs/editor/workspaces/multi-root-workspaces)

---

- Here is my local vscode setup structure. This post is preference based and may not be the right choice for every dev and it's working for me so far.

- **Goal:**
  > Sync all the settings(windows, wsl, MultoRootLevel) -> LocalClone -> Remote Git
  [![Windows to WSL Symbolic Link](/assets/images/posts/vscode/local-setup/srk-local-setup-architecture-refined.png)](/assets/images/posts/vscode/local-setup/srk-local-setup-architecture-refined.png)
  Note: VS Code already has [Settings sync feature](https://code.visualstudio.com/docs/editor/settings-sync). But, I would like to have my own sync workflow.

---

- **Implementation**
  - Sync Windows Level `settings.json`
    - [Refer: WSL: Using A WSL symlink folder from Windows](https://stackoverflow.com/a/76181147/792580)
    - Open Powershell with administrative privilege
    - [![Windows to WSL Symbolic Link](/assets/images/posts/vscode/local-setup/windows-settings-json-ps-symlink.png)](/assets/images/posts/vscode/local-setup/windows-settings-json-ps-symlink.png)
  - Sync WSL Level `settings.json`
    - > ln -s ~/work/srk/mono-workspace/my-vscode-central-settings/windows-wsl-your-workspace-folder/wsl/settings.json /home/srk/.vscode-server/data/Machine/settings.json
  - Sync Multiroot workspace level settings
    - [![My VsCode LocalSetup in Windows](/assets/images/posts/vscode/local-setup/srk-symlinks-mapping.png)](/assets/images/posts/vscode/local-setup/srk-symlinks-mapping.png)
  **Note:** All the workspaces mentioned in the above image are Multi-root workspaces.
  - And these multi-root workspaces are categorized into

    - <u>Org Space(~/work/&lt;org&gt;)</u>
      - `mono-workspace-be`
        - Contains BackEnd projects
      - `mono-workspace-fe`
        - Contains FrontEnd projects
      - `srk-lab`
        - This is for quick proof of concepts.
        - This contains a `.local` dir which in itself is a local git repo that contains notes, learnings related to org/team specific projects.
    - <u>Personal Space(~/work/srk)</u>  
      - `mono-workspace`
        - This contains personal learning, and passion projects
        - Learning, All System Designs, Code challenges etc
        - This might contain open source projects and anything that I love to create
