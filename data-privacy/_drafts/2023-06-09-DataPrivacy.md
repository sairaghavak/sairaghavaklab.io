---
# This is called Front matter
layout: post
title:  "Data Privacy Overview"
date:   2023-06-09 10:45 IST
author: SaiRaghava K
categories: data-privacy
---

## Personal Data

- More than just your name and contact information
- Examples: Your IP Address, preferences, medical symptoms, and transactional history are just some examples of personal data
- Below are some categories of personal data  
  - Personally Identifiable Information(PII)
    - Very limited set of personal data that directly identifies an individual like name, address, social security number, AAdhar Number or other identifying number, telephone number, or email address
  - PseudoAnonymous Data
    - Any data that cannot identify an individual without additional info like a key or matching table that is kept separately and protected
  - Sensitive Personal Data
    - Example: Race, Political Opinion
    - Can cause harm to an individual if disclosed in an unauthorized manner
- Not Personal Data
  - If a person cannot be identified with the data, then it's not personal data
- Aggregate Data
  - It does not specifically identify individuals in the group of people that aggregate information describes

### References

1. [Information Privacy](https://en.wikipedia.org/wiki/Information_privacy)
