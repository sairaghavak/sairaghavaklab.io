# Releases: Current Stable release `v1.0.0`

- v0.1.0
  - A quick check of gitlab pages without using static site generators(SSG) like Jekyll
- v0.2.0
  - Jekyll SSG with minor tweaks and enabling jekyll specific CI/CD Pipeline to deploy to GitLabPages
- v0.3.0
  - Add License - **DONE**
  - Customize
    - Header - DONE (Untouched as of now, no much changes)
    - Body - DONE (Updated _layouts/home.html - removed rss subscribe)
    - Footer - DONE(Updated _layouts/footer.html - made it simple
  - Add pagination feature - **DONE**
- v0.4.0
  - Add excerpts - **DONE**
  - Apply my preferred styling - **DONE**
  - Add Search feature
    - Header realignment and styling - **DONE**
    - Functional Search - **DONE**
  - Tags or categories
    - Create categories page and list posts grouped by categories - **DONE**
    - Add individual categories link to each POST - **DONE**
    - Each post category link, should redirect to posts by category display - **DONE**
    - Update footer - **DONE**
- v1.0.0
  - Integrate `Giscus` - **DONE**
  - Display categories in sorted order - **DONE**
  - Display total number of posts under categories - **DONE**
  - Display count of posts under each category - **DONE**

## Release workflow

A new release is done if any of the following is true

- Whenever new features are introduced
- Bug fixes
- Version upgrades of `Jekyll` or any plugins

Content updates like posts addition, modification, deletion, and any other static content updates supporting the posts like adding images are not release candidates.

## How to run and load drafts

- `bundle exec jekyll serve --drafts`
  - Some time it would say that it's missing gems
    - Example:
      > Could not find public_suffix-5.1.1, ffi-1.17.0 in locally installed gems
      - What this error means is that these versions are declared in `Gemfile.lock` but these are not installed in the system.
      - Where are all these gems installed. To know that issue `gem environment` command and look for key `"INSTALLATION DIRECTORY"`
        - In my case it is: `/home/srk/.asdf/installs/ruby/3.1.3/lib/ruby/gems/3.1.0`
        - Now issue the command `bundle install`
    - To remediate do
      > bundle install
    - Once installed, you can verify the installation by cd'ing into the folder
      - Say `/home/srk/.asdf/installs/ruby/3.1.3/lib/ruby/gems/3.1.0`
      - And do `ls -lat` - Focus on the entries at the top and you should see the most recent gems installed on the given date ant timestamp.
- If it's auto-refreshing use the following command
  - `bundle exec jekyll serve --drafts --force-polling`
