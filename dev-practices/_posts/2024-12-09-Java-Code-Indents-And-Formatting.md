---
# This is called Front matter
layout: post
title: Enabling Google's Java Style Guide in IDEs
date: 2024-12-09 20:14 IST
author: SaiRaghava K
categories: dev-practices
---


When working on project where there are multiple contributors it's essential to define coding standards and follow unified Style Guide.

There a many Style Guides, but the one I follow is [google-java-format](https://github.com/google/google-java-format). I mostly use VSCode in which I have configured the [Run on Save](https://marketplace.visualstudio.com/items?itemName=emeraldwalk.RunOnSave#:~:text=This%20extension%20allows%20configuring%20commands%20that%20get%20run,Add%20%22emeraldwalk.runonsave%22%20configuration%20to%20user%20or%20workspace%20settings.) extension to auto-format Java code in Google Style on file save action.

But in the java world, the majority of developers use [Intellij](https://www.jetbrains.com/idea/). The goal of this post is to explain how to apply Google java format on file save action in Intellij IDE.

---

## Part I

1. Install google-java-format plugin in Intellij

    [![google-java-format plugin](/assets/images/posts/dev-practices/google-java-format/1.png)](/assets/images/posts/dev-practices/google-java-format/1.png)

2. Ensure that the google-java-format plugin option is checked as shown below.

    [![google-java-format plugin](/assets/images/posts/dev-practices/google-java-format/2.png)](/assets/images/posts/dev-practices/google-java-format/2.png)

3. Also, there is one last step as mentioned in the [documentation](https://github.com/google/google-java-format?tab=readme-ov-file#intellij-jre-config)

    Here is the excerpt of the same
    > goto Help→Edit Custom VM Options... and paste in these lines:
    >
    ```config
    --add-exports=jdk.compiler/com.sun.tools.javac.api=ALL-UNNAMED
    --add-exports=jdk.compiler/com.sun.tools.javac.code=ALL-UNNAMED
    --add-exports=jdk.compiler/com.sun.tools.javac.file=ALL-UNNAMED
    --add-exports=jdk.compiler/com.sun.tools.javac.parser=ALL-UNNAMED
    --add-exports=jdk.compiler/com.sun.tools.javac.tree=ALL-UNNAMED
    --add-exports=jdk.compiler/com.sun.tools.javac.util=ALL-UNNAMED
    ```
    >
    > After that restart the IDE

If you have followed the steps so far, now on every Java file save action in Intellij the code is formatted in Google Java Style.

Note: In my case I am using `Java21` and the `java` command works from shell:
> `java --version`
>
> `openjdk 21.0.5 2024-10-15 LTS`

This will ensure that on `*.java` file save the Google Style Guide is applied. But, Intellij has some editor settings like Tab size, Indents etc. We have to ensure that these settings are not conflicting with Google Style Guide, at least the indents and spaces. In the following section this will be covered.

---

## Part II

Intellij by default has 4 space indents configured for Java File extensions.

[![google-java-format plugin](/assets/images/posts/dev-practices/google-java-format/3.png)](/assets/images/posts/dev-practices/google-java-format/3.png)

We can update the settings of Tab Size and Indent to 2 to align with Google Java Style Guide settings. But, there would be resistance by developers to do all these settings manually in IDE and they'll start complaining, moreover while working on multiple projects it's not recommended to make these changes at IDE level.

The better way is to use [`.editorconfig`](https://editorconfig.org/) which is not just about indents but it has some other config options that will be interpreted by popular editors/IDEs like VSCode, Intellij, and will apply the configs accordingly.

Here is the sample `.editorconfig` that is inline with Google Java Style with 2 space indents

```config
[*]
indent_style=space
ident_size=2
end_of_line=lf
charset=utf-8
trim_trailing_whitespace=true
insert_final_newline=true
```

Note that the `end_of_line=lf` has some special meaning. It means Line Feed.

As developers work on different Operating systems like Mac, Windows, and Linux. Each platform applies different Line Endings to files.

For Example:
> LF(Line Feed) - Unix Style Line Ending. Mac and Linux follows this.  
> CRLF(Carriage Return(\r) Line Feed(\n)) - Windows Style Line Ending

So, the problem is while a developer commits a file `a.txt` from Mac and another developer pulls code in Windows and does a git diff, it highlights the diff at end of the file, and if s/he commits it overrides the eol as CRLF. This is not intended and required.

To prevent this unwanted commit, we use the config `eol=LF`

The `.editorconfig` will only enforce this at the editor level but while committing code it may not enforce.

To apply and enforce the a common `EOL` before every commit this can be configured in `.gitattributes`

Here is the sample `.gitattributes` file that will ensure that the eol is always `LF` for all the contributors.

`.gitattributes`

```config
* eol=lf
```

And, `* eol=lf` will ensure that the changes are applied to non-binary files(i.e., text files) and will not corrupt any binary files like images, videos etc. Git, IDEs(that read .editorconfig) are smart enough to only apply this attribute to text files. Binary files will be left untouched.

In summary, having `.editorconfig`, `.gitattributes` and `google-java-format` plugin tied to File save action in your IDEs(VSCode/Intellij) would ensure that consistent Java Coding Style is maintained in your project. This helps improve developer productivity.

- Makes it easy to review code and focus on essential diffs in PR.
- Ensures that the code is easily readable and maintainable over time.
- Improves health of the repository and makes it easier to work with others.

Next Read: [Enabling Spotless plugin](https://sairaghavak.gitlab.io/style-guide/2023/01/09/Spotless-Maven-Plugin.html)

### References

1. [EditorConfig](https://editorconfig.org/)
2. [text=auto in .gitattributes](https://stackoverflow.com/questions/21472971/what-is-the-purpose-of-text-auto-in-gitattributes-file?noredirect=1&lq=1)
