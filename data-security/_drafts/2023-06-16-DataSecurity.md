---
# This is called Front matter
layout: post
title:  "Data Security Overview"
date:   2023-06-16 11:45 IST
author: SaiRaghava K
categories: data-security
---

## Things to learn

- Why data security is important
  - report incidents to `abc`
- What we mean by data security
  - Confidentiality: Ensures the necessary level of secrecy is enforced, and unauthorized disclosure is prevented.
  - Integrity: Is upheld when the accuracy and reliability of information and information systems are provided, and unauthorized modification is prevented.
  - Availability: Refers to the right people having access to the correct information and systems, reliably and consistently.
- The consequences of data security breach
  - Financial Loses
  - Reputational Damage
  - Loss of client trust
  - Loss of data
  - Theft
  - Stolen Identity
  - Damaged Intellectual Property
  - Possible Disciplinary actions
- Protecting data wherever you are working
  - Always lock your screen when leaving laptop or work are unattended
- How to create a secure password
  - Create password with a minimum of 8 characters, comprised of upper- and lower-case letters, numbers and symbols.
  - Create unique passwords based on a phrase that is easy for you to remember but difficult for others to guess and surrounded by numbers or symbols. Remember long is strong!
  - Avoid creating passwords with easy to find information about yourself or PI found out about on the internet.
  - Create unique passwords for each account and avoid writing them down.
  - Make sure no one is watching you enter or change your password.
  - Change your password immediately if you think it's been compromised and report it by emailing to `abc`
- How to use cloud solutions securely
  - Use approved cloud solutions
- How to use Social Media Securely
  - Make sure you understand what information your social media apps are collecting and sharing.
  - Never post any confidential data on social media unless authorized.
  - Create a strong and unique password for each social media account.
  - Make sure you update your social media privacy settings to manage who can follow and view your posts
  - Review each connect request carefully and always check if it's genuine before connecting.
  - Set up multifactor authentication if available.
  - Avoid buying friends or followers and avoid clicking obscure links.
- How to identify and avoid malware
  - Avoid free downloads. Install software from trusted sources.
  - Only open emails from trusted senders.
- How to secure laptops and mobile devices
  - Use strong password for your laptops and mobile devices.
  - Always use MFA(Multi-factor authentication)
  - Make sure your device or laptop screen is not visible to others.
- How to recognize and avoid phishing scams
  - Watch your emails that urge you to take action or employ scare tactics.
  - Watch out for emails that request personal information and contain poor spelling or grammar.
  - Avoid emails that contain a link that appears to point to one site; however, when hovering over it, it takes you somewhere else.
  - Steer clear of emails that declare you a winner of a competition you haven't entered.
  - Report an email that states it is from a national income tax agency saying a refund is due or appears to be sent from a government agency containing false threats.
  - Watch out for emails that ask for credit card donation after tragedy or natural disaster
  - Avoid emails that appear to be sent by a "friend" stranded in another country, requesting that you send money urgently via Western Union or some other untraceable means.
  - Report suspicious emails by clicking on the Report message button in outlook.

### References

1. [Data security](https://en.wikipedia.org/wiki/Data_security)
