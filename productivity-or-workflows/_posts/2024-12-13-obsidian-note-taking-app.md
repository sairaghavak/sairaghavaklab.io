---
# This is called Front matter
layout: post
title: Sync Obsidian Notes between Android and Linux
date: 2024-12-13 00:22 IST
author: SaiRaghava K
categories: productivity-or-workflows
---


The goal of this post is to setup a data sync workflow of Obsidian notes between Android and Linux.

- [![Obsidian Sync Workflow](/assets/images/posts/productivity-or-workflows/obsidian-workflow-linux-mint-screenshot.png)](/assets/images/posts/productivity-or-workflows/obsidian-workflow-linux-mint-screenshot.png)

---

## 1. Install [DropSync](https://play.google.com/store/apps/details?id=com.ttxapps.dropsync) App on Android

This app syncs a folder on mobile device with a folder in DropBox
- Here is my setup of DropSync
    - > Source <-> DropSync <-> Destination
    - > Folder on Mobile Disk <-> DropSync <-> Folder in Dropbox
    - > /storage/emulated/0/DropsyncFiles(This is the absolute path of the folder in my mobile device) <-> DropSync <-> /DropsyncFiles(This is the absolute path of the Dropbox folder in my mobile device)
- ![DropSync](/assets/images/posts/productivity-or-workflows/2.png)


---

## 2. Install [Obsidian](https://obsidian.md/) on Android

- Launch Obsidian 
    - > -> Create new vault ->  Enter vault name as `srk-obsidian-vault` -> Vault location
    - Select vault location as `/storage/emulated/0/DropsyncFiles`
	- Now, the absolute path of obsidian vault in android device is
		- `/storage/emulated/0/DropsyncFiles/srk-obsidian-vault`
- Select Vault folder and open in Obsidian. Sample screenshot below having a file in my obsidian vault.
- ![Obsidian](/assets/images/posts/productivity-or-workflows/1.png)

---

## 3. Install [Obsidian](https://obsidian.md/) on Linux

- **Prerequisite:** DropBox already installed in Linux Mint
- Download `*.deb`  version of Obsidian in my Linux Mint and install
  - ![Obsidian Deb installer](/assets/images/posts/productivity-or-workflows/3.png)
- Install Obsidian
  - ![Obsidian Installer](/assets/images/posts/productivity-or-workflows/4.png)
- Finally, click on the vault folder on the left pane and start writing your notes. :)

---

- **Publishing notes from Linux**
  - When contributing from Laptop your vault folder is in Dropbox and for every keystroke in `*.md` file of Obsidian application the file is synced to Dropbox
- **Publishing notes from Android**
  - But, while contributing from Android device, the data is stored in your device file structure i.e., 
    - `/storage/emulated/0/DropsyncFiles/srk-obsidian-vault` 
    - And DropSync will sync this folder with your folder in Dropbox i.e., `/storage/emulated/0/DropsyncFiles` based on the sync settings. 
	- The default auto sync schedule for every `2 hours`.
	- If you want to sync immediately Goto DropSync App and at the top right click on Sync ICON that will start syncing immediately. 
    - Workflow from Android Device
        - Do manual sync from DropSync app - Downloads if there are changes in Dropbox folder
        - Do your edits in Obsidian
        - Again do manual Sync in DropSync app - Uploads if there are changes in your device folder
        - > It's just like git pull, git commit, and git push

---


### References

1. [Obsidian](https://obsidian.md/)
2. [DropSync Android App](https://play.google.com/store/apps/details?id=com.ttxapps.dropsync&hl=en-US)
3. [Obsidian in Android to DropBox](https://forum.obsidian.md/t/mobile-syncing-vault-with-dropsync-on-android/20714)
4. [What is /storage/emulated/0/?](https://android.stackexchange.com/a/205434)
