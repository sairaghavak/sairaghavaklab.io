---
# This is called Front matter
layout: post
title:  "Overview of Spring Boot"
date: 2023-02-14 21:20 IST
author: SaiRaghava K
categories: springboot
---

- Cover at surface level on the following topics
  - Transaction Management
- Properties injection and profiles feature in Spring Boot?
  - Hierarchy of properties injection
    - JVM Args
    - Env vars
    - Properties within the application



**References:**

1. [Maven Dependency Scopes](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html#dependency-scope)
2. [Transitives dependencies scope for `provided` dependency](https://stackoverflow.com/questions/47651862/maven-scope-provided-and-transitive-dependencies)
