---
# This is called Front matter
layout: post
title:  "Java8 Vitals"
date:   2023-08-23 01:25 IST
author: SaiRaghava K
categories: java
---

- `XXXFunction`. It accepts a primitive and returns an object.
  - Example:
    - `IntFunction<R>`, takes int as argument and returns R.
    - Has `apply(int)`

- `ToXXXFunction`: Accepts any object and returns an int primitive.
  - Example:
    - `ToIntFunction`: Returns an `int`.
    - Has `applyAsInt(Any type)`


- **Basics:** A stream pipeline is not executed until a terminal operation is provided.

- **PartitioningBy Example**
  - 

- **GroupingBy Example**  

- **FlatMap Example**
  - 

- **Count**  

- **Max**

- **peek() Example**

- **sequential() vs parallel() in streams**