---
# This is called Front matter
layout: post
title:  "Java8 Feature Overview"
date:   2023-01-13 20:40 IST
author: SaiRaghava K
categories: java
---

Here is what chatgpt has to say

Java 8 is a major release of the Java programming language that introduced several new features and improvements. Some of the key features of Java 8 include:

1. Lambda Expressions: A new syntax for functional interfaces that allows for more concise and readable code when using functional programming constructs.
2. Method references: A new way to call methods, similar to lambda expressions, but it allows to refer to an existing method by name.
3. Functional Interfaces: A new type of interface that has a single abstract method, which is used as the basis for lambda expressions and method references.
4. Stream API: A new API for working with collections of data that allows for functional-style operations such as filtering, mapping, and reducing.
5. Date and Time API: A new API for working with dates and times that replaces the old and confusing `java.util.Date` and `java.util.Calendar` classes.
6. Default and Static Methods in Interfaces: Allow interface to have default or static methods which provides flexibility in evolution of interfaces.
7. Optional: A new container type that can be used to represent the absence of a value.
8. Nashorn JavaScript Engine: A new JavaScript engine that allows Java developers to run JavaScript code within a Java Virtual Machine (JVM).
9. Concurrent Accumulators: A set of new classes that can be used to safely accumulate values across multiple threads.
10. Parallel Array Sorting: A new method for sorting arrays that can take advantage of multiple processors to improve performance.
11. Base64 Encoding and Decoding: A new class for encoding and decoding binary data to and from a Base64 representation.
12. PermGen Space Replaced with Metaspace: PermGen is removed and replaced with Metaspace for better handling of class loading and unloading.

## Interface: default, static and private methods

- Default: A default method is a method that is defined in an interface with a default implementation, which means that implementing classes can use the default implementation or provide their own implementation. Default methods are useful for providing a default behavior for an interface method that can be overridden by an implementing class if needed.
- Example:
  
  ```java
  public interface Vehicle {
    default void drive() {
        System.out.println("Diriving vehicle");
    }
  }
  ```

  An implementing class can choose to override the default implementation of drive() or use the default implementation

  ```java
  public class Car implements Vehicle {
    public void drive() {
        System.out.println("Driving car");
    }
  }

  public class Motorcycle implements Vehicle {
    // Uses default implementation
  }
  ```

- Static: A static method in an interface is a method that can be called directly on the interface without creating an instance of a class that implements the interface. Static methods are useful for providing utility methods that are related to the interface but do not depend on the state of an instance. For example consider an interface called `MathematicalOperations` that has a static method `add()`
- Example:
  
  ```java
    public interface MathematicalOperation {
        static int add(int a, int b) {
            return a+b;
        }
    }
  ```

  The `add()` method can be called directly on the interface, without creating an instance

  ```java
   int result = MathematicalOperations.add(5,10);
  ```

- Private: A private method in an interface is a method that is only accessible within the interface and cannot be accessed by implementing classes or other code outside the interface. Private methods are useful for breaking down large or complex default methods into smaller, more manageable methods that can be reused within the interface. For example, consider an interface called `shape` that has a default method `calculateArea()`

  ```java
  public interface Shape {
    default double calculateArea() {
        return getLength() * getWidth();
    }

    private double getLength() {
        return 10.0;
    }

    private double getWidth() {
        return 5.0;
    }
  }
  ```

  The `getLength()` and `getWidth()` methods are private methods that are used within the `calculateArea()` method to calculate the area of the shape. These private methods are not accessible outside the interface, which helps to keep the interface's implementation details hidden and maintainable.

## Stream API: Java8 SQL like Operations

SQL operations like filtering, sorting, grouping, and aggregating data can be performed on Java collections using the Stream API. The Stream API provides several intermediate and terminal operations that allow you to perform these operations on collections in a declarative manner.

Here are some examples of how Java 8 Stream API operations can be used to perform SQL-like operations:

1. *Filtering data:* The filter method can be used to select elements from a collection that satisfy a given condition. For example:
  
   ```java
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
    List<Integer> evenNumbers = numbers.stream().filter(n -> n % 2 == 0).collect(Collectors.toList());
    ```

    This code will filter the even numbers from the list of numbers using the `filter` method.

2. *Sorting data:* The sorted method can be used to sort the elements in a collection. For example:

   ```java
   List<String> names = Arrays.asList("John", "Mary", "Bob", "Alice");
   List<String> sortedNames = names.stream().sorted().collect(Collectors.toList());
   ```

   This code will sort the names alphabetically using the sorted method and collect them into a new list.

3. *Grouping data:* The groupingBy method can be used to group elements in a collection based on a given criteria. For example:

   ```java
   List<String> names = Arrays.asList("John", "Mary", "Bob", "Alice");
   Map<Character, List<String>> groupedNames = names.stream().collect(Collectors.groupingBy(s -> s.charAt(0)));
   ```

4. *Aggregating data:* The sum, max, min, and count methods can be used to perform various aggregation operations on a collection. For example:

   ```java
   List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
   int sum = numbers.stream().mapToInt(Integer::intValue).sum();
   int max = numbers.stream().mapToInt(Integer::intValue).max().orElse(0);
   int min = numbers.stream().mapToInt(Integer::intValue).min().orElse(0);
   long count = numbers.stream().count();
   ```

**Note:** In Java 8, streams are not reusable. Once a terminal operation has been performed on a stream, it is considered consumed and cannot be used again. If you need to perform multiple operations on the same data, you can create a new stream from the original data source.

For example, let's say you have a list of integers and you want to find the sum of all even numbers in the list:

```java
List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

int sum = numbers.stream()
                .filter(n -> n % 2 == 0)
                .mapToInt(Integer::intValue)
                .sum();
```

In this code, the stream() method creates a new stream from the numbers list. The stream is filtered to only include even numbers, mapped to int values, and then summed up. Once the sum() terminal operation is called, the stream is consumed and cannot be reused.

If you need to perform the same operations on multiple lists of integers, you can create a method that takes a list of integers and returns the sum of all even numbers:

```java
public static int sumOfEvenNumbers(List<Integer> numbers) {
    return numbers.stream()
                  .filter(n -> n % 2 == 0)
                  .mapToInt(Integer::intValue)
                  .sum();
}
```

## SQL Operations and Java8 Equivalents

- Let's consider a simple dataset of employees with their names, salaries, and departments:

  ```sql
    Name        Salary   Department
    Alice       50000    HR
    Bob         70000    Finance
    Charlie     60000    HR
    Dave        90000    Finance
    Eve         80000    HR
  ```

- Here's an example of how to use SQL GROUP BY to group the employees by department and calculate the total salary for each department:

  ```sql
  SELECT Department, SUM(Salary) 
  FROM Employees 
  GROUP BY Department;
  ```

- And here's an equivalent example using Java 8 streams to achieve the same result:

  ```java
  List<Employee> employees = Arrays.asList(
    new Employee("Alice", 50000, "HR"),
    new Employee("Bob", 70000, "Finance"),
    new Employee("Charlie", 60000, "HR"),
    new Employee("Dave", 90000, "Finance"),
    new Employee("Eve", 80000, "HR")
  );

  Map<String, Integer> totalSalariesByDept = 
  employees.stream()
           .collect(
             Collectors.groupingBy(
                Employee::getDepartment, 
                Collectors.summingInt(Employee::getSalary)
            ));

  ```

- **GroupBy in SQL**:
  
  ```sql
  SELECT product_name, SUM(quantity * price) as total_sales
  FROM orders
  GROUP BY product_name;
  ```

  This will group the rows in the orders table by product_name column, and then calculate the total sales for each product using the SUM aggregate function.

  The output of the above SQL query might look like:

    ```yaml
    product_name  | total_sales
    --------------+------------
    Laptop        | 3500
    Smartphone    | 1200
    Headphones    | 400
    ```

  We can achieve the same result using Java8's stream() API and Collectors.groupingBy() method as shown below

  ```java
  List<Order> orders = new ArrayList<>();
  // populate orders list with data

  Map<String, Double> salesByProduct = orders.stream()
    .collect(
        Collectors.groupingBy(
            Order::getProductName, 
            Collectors.summingDouble(
                o -> o.getQuantity() * o.getPrice()
            )
        )
    );

  ```

## One liner SQL statements and their Java8 equivalents

1. Sql Query
  
    ```sql
    SELECT * FROM Employee 
    WHERE salary > 50000 
    ORDER BY name ASC;
    ```

    Java8 equivalent

    ```java
    employees.stream()
            .filter(e -> e.getSalary() > 50000)
            .sorted(Comparator.comparing(Employee::getName))
            .collect(Collectors.toList());

    ```

2. Sql Query
  
    ```sql
    SELECT COUNT(*) FROM Employee;
    ```

    Java8 equivalent

    ```java
    long count = employees.stream().count();
    ```

3. Sql Query

    ```sql
    SELECT DISTINCT department FROM Employee;
    ```

    Java8 equivalent

    ```java
    List<String> departments = employees.stream()
                                        .map(Employee::getDepartment)
                                        .distinct()
                                        .collect(Collectors.toList());
    ```

4. Sql Query

   ```sql
   SELECT MAX(salary) FROM Employee;
   ```

   Java8 equivalent

   ```java
   OptionalDouble maxSalary = employees.stream()
                                     .mapToDouble(Employee::getSalary)
                                     .max();
   ```

5. Sql Query

   ```sql
   SELECT * FROM Employee WHERE name LIKE 'J%';
   ```

   Java8 equivalent

   ```java
   employees.stream()
         .filter(e -> e.getName().startsWith("J"))
         .collect(Collectors.toList());
   ```

6. Sql Query

   ```sql
   SELECT * FROM Employee WHERE department IN ('IT', 'Finance');
   ```

   Java8 equivalent

   ```java
   List<Employee> filteredEmployees = employees.stream()
                                             .filter(e -> e.getDepartment().equals("IT") || e.getDepartment().equals("Finance"))
                                             .collect(Collectors.toList());
   ```

7. SQl Query

   ```sql
   SELECT name, salary FROM Employee 
   WHERE salary BETWEEN 40000 AND 60000;
   ```

   Java8 equivalent

   ```java
   Map<String, Double> nameSalaryMap = employees.stream()
                                             .filter(e -> e.getSalary() >= 40000 && e.getSalary() <= 60000)
                                             .collect(Collectors.toMap(Employee::getName, Employee::getSalary));
   ```

8. SQl Query

   ```sql
   SELECT AVG(salary) FROM Employee WHERE department = 'Sales';
   ```

   Java8 equivalent

   ```java
   OptionalDouble averageSalary = employees.stream()
                                          .filter(e -> e.getDepartment().equals("Sales"))
                                          .mapToDouble(Employee::getSalary)
                                          .average();
   ```

9. SQl Query

   ```sql
   SELECT * FROM Employee WHERE hireDate >= '2022-01-01' AND hireDate <= '2022-12-31';
   ```

   Java8 equivalent

   ```java
   employees.stream()
         .filter(e -> e.getHireDate().isAfter(LocalDate.of(2022, 1, 1)) && e.getHireDate().isBefore(LocalDate.of(2022, 12, 31)))
         .collect(Collectors.toList());
   ```

10. SQl Query

    ```sql
    SELECT COUNT(*) FROM Employee GROUP BY department;
    ```

    Java8 equivalent

    ```java
    Map<String, Long> departmentEmployeeCount = employees
    .stream()
    .collect(
        Collectors.groupingBy(
        Employee::getDepartment, 
        Collectors.counting()
        )
    );
    ```

---

### References
