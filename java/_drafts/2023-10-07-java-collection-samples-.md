---
# This is called Front matter
layout: post
title:  "Java Collection Samples"
date:   2023-10-07 23:40 IST
author: SaiRaghava K
categories: java
---


- Create a list

  ```java
  java.util.List<String> cities = new java.util.ArrayList();
  cities.add("bengaluru");
  cities.add("hyderabad");
  cities.add("chennai");
  cities.add("hong kong");
  ```

- Mutate a list
  - Example: insert a new city "c1" after a city whose name starts with "c"

    ```java
    for (String city: cities) {
      if (city.startsWith("c")) {
        cities.add("c1") ; 
        // results in java.util.ConcurrentModificationException
        //cities.remove(city); // works
      }
    }
    ```

  - For list mutation recommended to use `java.util.Iterator`

    ```java
    java.util.List<String> cities = new java.util.ArrayList();
    cities.add("bengaluru");
    cities.add("hyderabad");
    cities.add("chennai");
    cities.add("hong kong");

    ListIterator<String> cityItr = cities.listIterator();
    while (cityItr.hasNext()) {
      String city = cityItr.next();
      if (city.startsWith("h")) {
        cityItr.remove();
        cityItr.add("-");
      }
    }
    
    System.out.println(cities);
    ```

- Creating a read-only list

  ```java
  java.util.List<String> cities = java.util.Arrays.asList("blr", "hyd", "chn");
  ```

  - Any attempt to modify the city would result in `java.lang.UnSupportedOperationException`
    - Example1:

      ```java
      cities.add("delhi"); // Results in java.lang.UnSupportedOperationException
      ```
