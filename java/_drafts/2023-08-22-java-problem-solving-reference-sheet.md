---
# This is called Front matter
layout: post
title:  "Java Reference Sheet for Problem Solving"
date:   2023-08-22 02:02 IST
author: SaiRaghava K
categories: java
---

This is a quick and non-exhaustive list of Java reference tips for problem-solving

---

---

- Given a number get the right most digit: Use `%`(modulus) operator with denominator as 10.

### Strings

- [`java.lang.String`](https://docs.oracle.com/javase/8/docs/api/java/lang/String.html)
  - static methods

  - String.join("delimiter", "CharSequence") - static method introduced in Java1.8
  - String.join("delimiter", "Iterable<CharSequence>") - static method introduced in Java1.8

  - String class notable methods
    - `.indexOf(char)`
    - `.length()`
    - `.charAt(index)`
    - `.isEmpty()` // Since 1.6
    - `.isBlank()` // Since Java 11
    - `.toUpperCase()`
    - `.toLowerCase()`
    - `.startsWith()`
    - `.endsWith()`
    - `.equals(str)`
    - `.equalsIgnoreCase(str)`
    - `.compareTo(str)` // returns int compare 2 strings lexicographically
    - `.compareToIgnoreCase(str)`// returns int compare 2 strings lexicographically
    - `.contains(str)`
    - `.replaceAll(regex, replacement)` // since java 1.4
    - `.repeat(int)` - return String - Since Java11
    - `.split("delimeter")` - returns String[]
    - `.substring(index)` - **Note:** Generally you write subString but remember there is no camel case
    - `.substring(startIndexInclusive, endIndexExclusive)`
      - Example:

        ```java
          String x = "test"; // len = 4 => index 0 to 3
          System.out.println(x.substring(0, 4)); // endIndex val should be <= len of str
        // if endIndex val is > str.length() it wil throw java.lang.StringIndexOutOfBoundsException  
        ```

    - `.toCharArray()` - returns char[]
    - `.chars()` - introduced in java9 **returns `java.util.stream.IntStream`**: Example `"name".chars().distinct().count()`;
      - Example: To find the count of chars e in `String x = "heleleo";` use
        - `long count = x.chars().mapToObj(i -> (char)i).filter(elm -> elm == 'e').count();`
    - `.concat(str)`
    - `.trim()` // trims the starting and trailing whitespces

---

### Find if a character is a lower case alphabet
  
- ```java
    if (c >= 'a' || c <= 'z') { // Does ASCII Decimal value comparison
      // char is any of a-z
    }
    ```

- Refer: [https://www.ascii-code.com/](https://www.ascii-code.com/)

- Find if a character is letter or digit
  - `java.lang.Character.isLetterOrDigit('c')`;
- Find if a character is lowercase
  - `java.lang.Character.isLowerCase(char)`  
- Find if a character is uppercase
  - `java.lang.Character.isUpperCase(char)`
- Find if a character is digit
  - `java.lang.Character.isDigit(char)`

### Wrapper Classes

- To know if the number is within the int range Integer wrapper class has constants
  - `Integer.MIN_VALUE` // -2147483648
  - `Integer.MAX_VALUE` // 2147483647

### `java.lang.StringBuilder`

- `.append('char')`
- `.append("String")`
- `.toString()` - converts StringBuilder to string
- `.reverse()`// returns a StringBuilder
-

### `java.lang.Math` package

- `Math.floor(10.5)` - 10.0 - takes double, returns double,
- `Math.round(10.5)` - 11 - return long if param is double, and int if param is float
- `Math.ceil(10.1)` - 11.0 - takes double, returns double
- `Math.abs(-25)` - 25 - takes int, returns int

### `java.util.Map`

- `.values()` // returns Collection<ValueType>
- `.keySet()` // returns Set<KeyType>
- `.containsKey(key)`
- `.containsValue(value)`
- `.put(key, val)`
- `.putIfAbsent(key, val)`
- `.get(key)`
- `.getOrDefault(key, defaultVal)`
- `remove(key)` // returns the value associated with the removed key
- `.entrySet()`

### Java8

- Filter and count
  - filter().count() - this returns primitive type long
- List<T>
  - `void replaceAll(n -> n+1)` // calls the lambda once for each item in the list, storing the result back into the list(or other type of collection)
  - `boolean removeIf(predicate)` // calls the lambda once for each item in the collection, removing each item where lambda returns boolean

- String x = "hello";
  - x.chars() // Since Java9 returns IntStream

### `java.util.Arrays` Class

- `Arrays.stream()` // takes int[], long[], double[] and returns IntStream, LongStream, DoubleStream
- Convert Stream<int[]> to Stream<Integer>
  - `IntStream.of(nums).boxed();` // returns Stream<Integer>
- `Arrays.copyOfRange(sourceArr, startIndexInclusive, endIndexExclusive)` // returns type of sourceArr
- `Arrays.toString(intArr)` // to print the content of the array

### `java.util.stream` package

- Stream.

### `java.util.function` package

### Arrays

- One dimensional arrays also known as Linear Arrays

### Printing Arrays

- `Arrays.toString()`// Has overloaded methods which takes argument as byte, short, int, long, float, double, boolean, char, Object
