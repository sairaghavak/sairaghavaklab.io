---
# This is called Front matter
layout: post
title:  "Java Language Refresher"
date:   2023-09-21 12:30 IST
author: SaiRaghava K
categories: java
---

- Java is a **CASE-SENSITIVE** language.
  - Java compiler rants when you say `string x = "hero";` instead of `String x = "hero";`
- Statically typed language
  - While declaring a variable we define its data type like `int x = 10;`. Here the type of the variable is determined at compile time so the name statically typed language.
  - While in dynamically typed languages like python we can say

    ```python
    x = 10
    x = "srk"
    ```

  - Here the type of the variable is resolved at runtime

- It's primarily an Object-Oriented Programming Language with Functional programming features introduced starting Java8

---

### Fundamental Data Types

|data type | size | default val|
| --- | --- | --- |
|`byte` | 1 byte | **0** |
| `short` | 2 bytes | **0**  |
| `int` | 4 bytes |  **0** |
| `long`| 8 bytes | **0L**  |
|`float` | 4 bytes | **0.0f**  |
|`double` | 8 bytes | **0.0d**  |
| `char` | 2 byte | **'\u0000'** <br> to check if char has default value use  <br> `if(c == 0) {}` |
|`boolean` | [Depends on the VM](https://stackoverflow.com/a/383554/792580) | **false** |

---

## `java.lang.*` - Some important Types(Classes and Interfaces)

- `java.lang.Number` - Abstract class
- `java.lang.Comparable<T>` - Interface has one abstract method `public int compareTo(T o);`

- Wrappers
  - `java.lang.Byte extends Number implements Comparable<Byte>`
  - `java.lang.Short extends Number implements Comparable<Short>`
  - `java.lang.Integer extends Number implements Comparable<Integer>`
  - `java.lang.Long extends Number implements Comparable<Long>`
  - `java.lang.Float extends Number implements Comparable<Float>`
  - `java.lang.Double extends Number implements Comparable<Double>`
  ---
  - `java.lang.Character implements Comparable<Character>`
  ---
  - `java.lang.Boolean implements Comparable<Boolean>`
- `java.lang.CharSequence` - Interface
  - `java.lang.String implements Comparable<String> CharSequence`

- `java.lang.Math`
  - `Math.PI` - PI constant (22/7)
  - `Math.min(a, b)` - a, b can be int, long, float, double
  - `Math.max(a, b)` - a, b can be int, long, float, double
  - `math.pow(double a,double b)` - returns double
  ---
  - `Math.abs(a)` - a can be int, long, float, double
  - `Math.sqrt(a)` - a can be int, long, float, double
  - `Math.floor(double a)`
  - `Math.ceil(double a)`
  - `Math.round(double a)` - returns long
  - `Math.round(float a)` - returns int

## `java.math.*` - for Fundamental Math operations

- `java.math.BigInteger` - When you need to work with very large integers that exceed the range of Java's primitive data types like `int` and `long`
- `java.math.BigDecimal` - Used for high-precision decimal calculations to void rounding errors, for financial/tax/currency-conversion calculations. Also, use it when you think the values exceed the range of floating point primitive data type like `float` and `double`.

### Access Modifiers

- Most restrictive to least
  - `private` - accessible only within the class
  - `default` - package private. When no access modifier is specified the `default` is applied. No explicit declaration is required.
  - `protected` - package private + subclass from another package
  - `public` - accessible to the world

---

### Non-Access Modifiers

- `static`
  - `static` variables or class variables
    - Example: `static String x = "srk";` // aka class level variables
      - How to access? `<classname>.x`, Example: `System.out.println(A.x);`
  - `static` methods
    - Example:

      ```java
      public class A {
        static int x;
        int y;
        static void m1() {
          System.out.println(x); // Allowed
          // System.out.println(y); // Doesn't compile
          /*-
          Error:
          cannot make a static reference to a non-static field y
          */
          // m2(); // Doesn't compile
          /*-
          Error:
          cannot make a static reference to the non-static method m2()
          */
        }

        void m2() {
          m1(); // Allowed
          System.out.println(x); // Allowed
          System.out.println(y); // Allowed
        }
      }
      ```

  - `static` classes aka nested classes. Refer Nested Classes section below.

- `final`
  - **TODO**
- `abstract`
  - **TODO**
- `synchronized`
  - Cannot be applied to class
  - Can be applied to methods and code blocks
  - **TODO**

---

### `class` keyword

- Hello World program

  ```java
  public class FirstJavaProgram {
    public static void main(String[] args) {
      System.out.println("Hello World");
    }
  }
  ```

- The main class should be public and should match the filename say `FirstJavaProgram.java`
- *Note:* An enclosing class cannot be `static` but nested classes can be static.

---

### Nested Classes - Advanced Topic - Skip if you're a beginner

- A class declared within the enclosing class
  - **<u>static nested class</u>**
    - Example:

      ```java
      public class A { // enclosing class
        static int aX;
        int aY;
        /*-
        A static nested class for the outer class is just another class member

        All rules applicable for any class members like fields, 
        methods are applicable to static nested classes as well
        */
        static class Nested  { // it's a static member of class A
          // ONLY static data of enclosing class is visible here

          static int x;
          int y;

          static void m1() {
            // can access ONLY static data of nested(static-inner) class
            System.out.println(x);
            // System.out.println(y); Doesn't compile
            // y can be accessed here using -> new Nested().y;

            // can access ONLY static data of outer class
            System.out.println(aX);
            // System.out.println(aY); // not directly visible here
            // But, can access it as System.out.println(new A().aY); 
          }
          void m2() {
            // can access data of nested(static-inner) class
            System.out.println(x);
            System.out.println(y);
            // can access ONLY static data of outer class
            System.out.println(aX);
            // System.out.println(aY); // not directly visible here
            // But, can access it as System.out.println(new A().aY); 
          }
        }
      }
      ```

    - Static data of static nested class, can be accessed using `A.Nested.m1();` or `A.Nested.x;`
    - Non-static data of static nested class, can be accessed by creating object of static nested class

      ```java
      A.Nested nested = new A.Nested();
      nested.m2(); // to access non-static data of Nested(static-inner) class
      ```

  - *<u>When to use static nested class</u>*
    - Use it when you want to restrict access to instance fields of the enclosing class within the static nested class. Or if you want to access only static fields of the enclosing class but not instance fields then static-nested class is preferred.
    - Also, use it when you want to instantiate the static-nested class without the need for creating instance of enclosing class. Example: [Builder Design Pattern](https://sairaghavak.gitlab.io/gof-design-patterns/2022/07/30/Builder.html)
    - When you want to organize the related utility classes within a larger class for better code organization. In other words, when you want to logically group a class within another class.
  - **<u>non-static nested classes aka inner class</u>**
    - 3 types

      - Instance level inner class
        - Example

          ```java
          public class A { // enclosing class
            static int aX;
            int aY;

            void aM1() {}

            static void aM2() {}

            class Inner { // it's a non-static member of class A
              // can access both static and non-static data of class A

              // static int x;
              // x cannot be declared static in a non-static inner type

              // only constants are allowed in inner(non-static) class
              static final int x = 10;
              int y;

              // static methods are not allowed in inner(non-static) class
              // static void m1() {}

              void m2() {
                // can access all data of inner(non-static inner) class
                System.out.println(x);
                System.out.println(y);
                // can access all data of outer class
                System.out.println(aX);
                System.out.println(aY);
                aM2();
                aM1();
              }
            }
          ```

        - No static data in inner(non-static) class
        - Non-static data of inner(non-static) class, can be accessed by creating object of inner class

          ```java
          // to access non-static data of inner(non-static) class
          A.Inner innerClass = new A().new Inner();
          innerClass.m2();
          ```

      - Method local inner class
        - Example:

          ```java
          public class A { // enclosing class

            private int aX;

            void m1() {

              int x = 10;

              // Class inside this method is just like local variable of this method

              /*-
              * All rules applied for a local variables are applicable for a local inner class
              * 1. access-modifiers are not applicable
              * 2. Non-access modifiers like static is NOT applicable
              *    either abstract or final are applicable but not both
              */

              class LocalInner {
                int y;

                // static final is allowed but no static stuff is allowed

                void m2() {
                  System.out.println(aX);
                  System.out.println(x);
                  System.out.println(y);
                }

                /*- Just like a local method variable this class scope is
                limited within method m1()*/
              }

              // calling m2() of LocalInner
              LocalInner li = new LocalInner();
              li.m2();
            }

            public static void main(String[] args) {
              A a = new A();
              a.m1();
            }
          }
          ```

      - Anonymous inner class
        - Example

          ```java
          class A {
            public void m1() {
              System.out.println("In m1");
            }
            public void m2() {
              System.out.println("In m2");
            }
          }
          public class B {
            A a = new A() { // Anonymous sub-type of A
              @Override
              public void m1() {
                System.out.println("In overridden m1")
              }
            };

            void print() {
              a.m1(); // calls overridden m1()
              a.m2(); // calls super class's inherited m2()
            }
          }
          ```

        - Used to implement interfaces, create subtypes of a class on the fly
  - *<u>When to use non-static nested class</u>*
    - Use it when you want to access both static and instance data of the enclosing class i.e., the inner class can directly access and change the state of the enclosing class
    - When you want to organize the related utility classes within a larger class for better code organization. In other words, when you want to logically group a class within another class.

### Creating Constructors

- Example:
  
  ```java
  public class Student {
    public Student() {}
    /*- Default constructor is implict 
    Not required to declare unless you want to log or initialize 
    or do some class startup activties
    */
    public Student(int id) {} // paramterized constructor 
  }
  ```

- Example with Single inheritance

  ```java
  class A {
    A() {
      System.out.println("A's constructor");
    }

  }
  public class B extends A{
    B() {
      super(); // implicit: not required to explicitly add super() here
    }

    B(int x) {
      super(); // implicit: not required to explicitly add super() here
    }


    public static void main(String[] args) {
     new B();
     new B(1);
     /*-
     Point is: 
     The default constructor or parameterized will have super() 
     as first line so that it calls super class constructor
     */
    }
  }
  ```

---

### `new` keyword

- `new` is a keyword in java
  - Example: `Student student = new Student();`
- Used to create objects in Java
- Alternatively, we can create objects using `java.lang.reflect` API. This is an advanced topic.

---

### Method Overloading

- Example:
  
  ```java
  public class Printer {
    public void print(String msg) {}
    public void print(int id) {}
  }
  ```

- Real-time example from JDK is `System.out.println()`
  - `System.out` resolves to instance of `java.io.PrintStream` class, and it has got overloaded `println()` methods like
    - `void println()`
    - `void println(boolean x)`
    - `void println(char x)`
    - `void println(char[] x)`
    - `void prinltn(int x)`
    - `void prinltn(long x)`
    - And more...

- Method overloading is the best example of Compile-time Polymorphism

---

### Method Overriding - Applied in inheritance usecase

- Example:

  ```java
  class A {
    void m1() {}
  } 
  class B extends A {
    @Override
    void m1() {}
  }
  ```

- Usage:
  
  1.

     ```java
     A a = new A();
     a.m1(); // calls A's m1()
     ```

  2.

    ```java
    B b = new B();
    b.m1(); // calls B's m1()
    ```

  3.

    ```java
    A a = new B();
    a.m1(); // calls B's m1()
    ```

- Method overriding is the best example of Runtime Polymorphism
- **Covariant Return Types**
  - Example:

    ```java
    abstract class X {
      abstract Number getVal();
    }

    class IntX extends X {
      @Override
      Integer getVal() {
        return 1;
      }
    }

    class FloatX extends X {
      @Override
      Float getVal() {
        return 5.9f;
      }
    }

    public class CovariantReturnTypes {

      public static void main(String[] args) {
        List<Number> xs = new ArrayList<>();
        xs.add(new IntX().getVal());
        xs.add(new FloatX().getVal());
        System.out.println(xs); // prints [1, 5.9]
      }
    }

    ```

---

### Exception Handling

- Java Exception class Hierarchy

---

### Logging

---

### Input/Output and Working with files

---

###

---

##

- **TODO**
- Come up with what constitutes as Java Basics?
- More basics to be added, and must be reviewed and proofread

---

**References:**
