---
# This is called Front matter
layout: post
title:  "Arrays"
date:   2023-05-17 08:57 IST
author: SaiRaghava K
categories: dsa-problem-solving
---

### Java: Basic Array operations

- Assume an arr `int[] arr = new int[5];`
  - Fetching the value at index i `arr[i]`
  - Assigning value at index i `arr[i]= 10`
  - `arr.length` - to find the size of the array
  - Print the array
    - `java.util.Arrays.toString(arr);`

---

### Terminology and algorithms/approaches used while solving array based problems

- > Contiguous Subarray - Is a contiguous sequence from a given source array
  - Example: Given arr = {1,2,3,4}
    - Subarray - is contiguous, ~~[1,3,4]~~ is not a contiguous subarray, and also ~~[1,3] or [1,4] or [2,4]~~
    - Subarrays at index 0
      - [1]
      - [1,2]
      - [1,2,3],
      - [1,2,3,4]
    - Subarrays at index 1
      - [2]
      - [2,3]
      - [2,3,4]
    - Subarrays at index 2
      - [3]
      - [3,4]
    - Subarrays at index 3
      - [4]

- [**Max sum subarray problem solution**](https://en.wikipedia.org/wiki/Maximum_subarray_problem)
  - > Problem Statement: Given an array, find a contiguous subarray(a subarray with consecutive elements) that has the largest possible sum.

  - **Breakdown and understanding the Problem**
    - Example: Given arr = {1,2,3}
    - Contiguous Subarrays at index 0
      - [1] - sum = 1
      - [1,2] - sum = 1+2 = 3
      - [1,2,3] - sum = 1+2+3 = 6
    - Contiguous Subarrays at index 1
      - [2] - sum = 2
      - [2,3] - sum = 2+3=5
    - Contiguous Subarrays at index 2
      - [3] - sum = 3
    - Goal of the problem: Find a contiguous sub array that has the largest possible sum
      - Therefore
        - Sub array with max sum of its elements = [1,2,3]
        - Max sub array sum = 6

  - **Solving it using kadanes algorithm** provides solution in `O(n) - Linear` time complexity
  
  - > Max subarray problem computation
    - Example 1: Given arr = {1, 2, 3}
      - Kadanes algorithm
        - Start from index 1
          - At index 1 possible sub arrays
          - [2], [1,2] => max(2, 3) => maxsubarrysum = 3
        - At index 2 possible sub arrays
          - [3], [2,3], [1,2,3] = max(3, 5, 6) => maxsubarrysum = 6
      - Finally, max(1,3,6) => maxsubarrysum = **6**
    - Example 2: Given arr = {-2, 3, 2, -1}
      - Kadanes algorithm
        - start from index 1
          - at index 1 possible sub arrays = [3], [-2,3] - maxsubarrsum at index 1 = max(3,1)=3
          - at index 2 possible sub arrays = [2], [3,2], [-2,3,2] - maxsubarrsum at index 2 - max(2,5,3) = 5
          - at index 3 possible sub arrays = [-1], [2,-1], [3,2,-1], [-2,3,2,-1] - maxsubarrsum at index 3- max(-1,1,4,2) = 4
        - Finally, max of all maxsums at each index = max(3,5,4) = **5**

---

### Few array problems

- [Leetcode: Two sum](https://leetcode.com/problems/two-sum/)
  - My Solution:

    ```java
    public int[] twoSum(int[] nums, int target) {
      int[] result = new int[2]; // return 2 sum with values
      /*- O(n^2))
            twosum:
            for (int i=0; i < nums.length; i++) {
              int val = nums[i];
              int searchVal = target-val;
              for (int j=i+1; j < nums.length; j++) {
                  if(nums[j] == searchVal) {
                      result[0] = i;
                      result[1] = j;
                      break twosum;
                  }
              }
            }
        */
      /*- O(n) - Linear complexity*/
      Map<Integer, Integer> map = new HashMap<>();
      for (int i=0; i<nums.length; i++) {
        int val = nums[i];
        int searchVal = target-val;
        // find if map already contains the searchval
        if (map.containsKey(searchVal)) {
            result[0] = map.get(searchVal); // returns index
            result[1] = i;
            break;
        }
        map.put(val, i); // Store in map {val=index-of-val}
      }
      return result;
    }
    

    ```

- [LeetCode: Running sum or prefix sum of 1d-array](https://leetcode.com/problems/running-sum-of-1d-array/)
  - My Solution:

    ```java
    int[] nums = {1,2,3,4};
    public int[] runningSum(int[] nums) {
      for (int i=1; i< nums.length; i++) { // start from index 1
      nums[i] = nums[i-1] + nums[i]; // sum at this index = prev index val + this index val
      }
    }
    // Time Complexity is O(n) - Linear
    // Space Complexity is O(1) - Constant
    ```

- [LeetCode max sub array sum](https://leetcode.com/problems/maximum-subarray/)
  - My Solution:

      ```java
      public int maxSubArray(int[] nums) {
        int maxsum = nums[0];
        for (int i=1; i<nums.length; i++) { // start from index 1
            // update val at current index with max subarray sum
            nums[i] = Math.max(nums[i-1] + nums[i], nums[i]); 
            maxsum = Math.max(maxsum, nums[i]);
        }
        return maxsum;
      }
      ```
  - TODO: How to get the max sub array instead of max sub array's sum

- [Rotate array to the right by k steps](https://leetcode.com/problems/rotate-array/)
  - Approach
    - Given array 1,2,3,4 rotate it by 2 steps
        > 1, 2, 3, 4 -- Give input
        >
        > Rotate 1st time
        >
        > _, 1, 2, 3
        >
        > 4, 1, 2, 3
        >
        > Rotate 2nd time
        >
        > _, 4, 1, 2
        >
        > 3, 4, 1, 2 -- Final result
  - But, rotating array by k times and shifting elements is not efficient solution
  - My Solution:
    - TODO - Apply better approach
- Find the maximum element in the array
- Find the minimum element in the array
- Calculate the sum of all elements in an array
- Reverse an array in-place
- Check if an array is sorted in non-descending order
- Remove the duplicates from a sorted array
- Find the nth largest element in an array
- Find the intersection of 2 arrays
- Check if an array contains specific element.

**References**

1. [Neetcode](https://neetcode.io/practice)
2. [CS Dojo](https://www.youtube.com/watch?v=86CQq3pKSUw)
3. [Nick White](https://www.youtube.com/watch?v=jnoVtCKECmQ)
4. [Max Sub Array problem](https://en.wikipedia.org/wiki/Maximum_subarray_problem)
