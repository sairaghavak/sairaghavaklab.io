---
# This is called Front matter
layout: post
title:  "#x Problem Solving Questions"
date:   2023-09-19 08:57 IST
author: SaiRaghava K
categories: dsa-problem-solving
comments: false
---

Here is a non-exhaustive list of problem-solving questions.

---

- Pre-read:
  - [Time and Space Complexity](https://sairaghavak.gitlab.io/dsa-problem-solving/2023/05/28/time-and-space-complexity.html)
  - [Big O CheatSheet](https://www.bigocheatsheet.com/)
    - Worst case scenario to be considered when benchmarking
  - If Java is your programming language to solve, here is the Collections framework overview chart
    - [![Collection Framework Overview Chart](/assets/images/posts/collections-framework/imgs/summary-of-collections.png)](/assets/images/posts/collections-framework/imgs/summary-of-collections.png)

---

#### Questions

---

|---|---|---|
|1|[fizz-buzz](https://leetcode.com/problems/fizz-buzz/)||
|2|[Warmup-2 > last2](https://codingbat.com/prob/p178318)|Question description is confusing. Focus on sample input and output. Count the last 2 chars substring in given string except the last 2chars|
|3|[Warmup-2 > arrayCount9](https://codingbat.com/prob/p184031)|HINT: Use 2-pointer|
|4|[Warmup-2 > countXX](https://codingbat.com/prob/p194667)||
|5|[Warmup-2 > stringX](https://codingbat.com/prob/p171260)|
|6|[Warmup-2 > has271](https://codingbat.com/prob/p167430)||
|7|[Warmup-2 > altPairs](https://codingbat.com/prob/p121596)|
|8|[String1 > minCat](https://codingbat.com/prob/p105745)|
|9|[String-1 > without2](https://codingbat.com/prob/p142247)|
|10|[String-1 > startWord](https://codingbat.com/prob/p136594)|
|11|[String-1 > withoutX](https://codingbat.com/prob/p193507)|
|12|[String-1 > withoutX2](https://codingbat.com/prob/p151359)|
|13|[minimum-moves-to-equal-array-elements](https://leetcode.com/problems/minimum-moves-to-equal-array-elements/description/)||
|14|[minimum-moves-to-equal-array-elements-ii](https://leetcode.com/problems/minimum-moves-to-equal-array-elements-ii/description/)||
|15|[longest-happy-string](https://leetcode.com/problems/longest-happy-string/description/)||
|16|[fibonacci-number](https://leetcode.com/problems/fibonacci-number/)||
|17|[reverse-integer](https://leetcode.com/problems/reverse-integer/)||

NOTE: In math division we have 2 parts - remainder and quotient. In programming we use  

- `%` operator to find the remainder and  
- `/` operator to find the quotient  
- Example Algorithm/pseudocode  

> Given a number 123
>
> Divide by 10
>
> 123 / 10 = 12(quotient)
>
> 123 % 10 = 3(remainder)
>
> initialize 3 vars reverse=0, remainder=0, number=123

```java
while(number != 0) {
    remainder = number % 10;
    reverse = reverse*10 + remainder;
    number /= 10;
}
```

  > Finally, the reverse var has the reversed value

|18|[gcd-of-two-numbers](https://practice.geeksforgeeks.org/problems/gcd-of-two-numbers3459/1)|Refer Algorithm: [Euclidian Algorithm](https://en.wikipedia.org/wiki/Greatest_common_divisor#Euclid's_algorithm)|
|19|[armstrong-number](https://leetcode.com/problems/armstrong-number/) <br> Alternative Link: [armstrong-numbers](https://practice.geeksforgeeks.org/problems/armstrong-numbers2727/1)||
|20|[palindrome-number](https://leetcode.com/problems/palindrome-number/) <br> Note: Solve it without converting the input to String ||
|21|[Give a string in form of char[] reverse it in place](https://leetcode.com/problems/reverse-string/) <br> Note: Solve it in-place | HINT: 2 pointers at front and back, break the loop when i < j and swap chars |
|22|[two-sum](https://leetcode.com/problems/two-sum/)||
|23|[three-sum](https://leetcode.com/problems/3sum/description/)||
|24|[contains-duplicate](https://leetcode.com/problems/contains-duplicate/)||
|25|[valid-anagram](https://leetcode.com/problems/valid-anagram/)||
|26|[Valid Parentheses](https://leetcode.com/problems/valid-parentheses/)|HINT: `java.util.Stack`(push, peek, pop) + `switch` statement|
|27|[kth-largest-element-in-an-array](https://leetcode.com/problems/kth-largest-element-in-an-array/) <br> Note: Solve it without Sorting | HINT: with `offer()` load the given array into PriorityQueue(ADT)(Initialize the priority queue as max-heap passing `Comparator.reverseOrder()`) and `poll()` the head k times |
|28|[best-time-to-buy-and-sell-stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description/)||
|29|[Reverse a Linked list](https://leetcode.com/problems/reverse-linked-list/description/)||

- Linked List Insertion: [![Linked List insertion logic](/assets/images/posts/dsa-problem-solving/linkedlist.png)](/assets/images/posts/dsa-problem-solving/linkedlist.png)
- Linked List Reversal: [![Linked List reversal logic](/assets/images/posts/dsa-problem-solving/reverse-a-ll.png)](/assets/images/posts/dsa-problem-solving/reverse-a-ll.png)

|30|[Remove Linked List elements](https://leetcode.com/problems/remove-linked-list-elements/description/)||
|31|[Move Zeros](https://leetcode.com/problems/move-zeroes/)||
|32|[Max Sub array sum](https://leetcode.com/problems/maximum-subarray/description/)||
|33|[Top k Frequent elements](https://leetcode.com/problems/top-k-frequent-elements/description/)||
|34|[Rotate array to right k steps](https://leetcode.com/problems/rotate-array/description/)||
|35|[LRU Cache](https://leetcode.com/problems/lru-cache/description/)||

- HINT:
  - Option1: `LinkedList`(Is a Doubly LinkedList in Java) + `HashMap`
  - Option2:
  
  ```java
    int capacity = 2;
    LinkedHashMap<Integer, Integer> lhs =
    new LinkedHashMap<>(capacity, 0.75f, true) {

      @Override
      protected boolean removeEldestEntry(java.util.Map.Entry<Integer, Integer> eldest) {
        return size() > capacity;
      }
    };
  ```

|36|[Reverse words in a string](https://leetcode.com/problems/reverse-words-in-a-string/)||
|37|[Reverse Words](https://www.codewars.com/kata/5259b20d6021e9e14c0010d4/java)||
|38|[Array-1 > unlucky1](https://codingbat.com/prob/p197308)



### References

1. [NeetCode practice](https://neetcode.io/practice)
