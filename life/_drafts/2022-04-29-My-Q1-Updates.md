---
# This is called Front matter
layout: post
title:  "2022-Q1 Updates"
date:   2022-04-29 13:00 IST
author: SaiRaghava K
categories: life
published: false
# Unpublihsed and moved to drafts folder
---

2022 is a year of hope because Covid cases have declined drastically. I made significant progress in Q1 of 2022. Started off by reading the book `The Subtle Art of Not Giving a F*ck: A Counterintuitive Approach to Living a Good Life` and that's an incredible book filled with unconventional practical wisdom. Thanks to my [great friend](https://twitter.com/AnimeshBulusu) who has recommended this book to me.

An excerpt from the book
> No matter where you go, there's a five-hundred-pound load of shit waiting for you. And that's perfectly fine. The point isn't to get away from the shit. The point is to find the shit you enjoy dealing with.

And read a couple of other books

- `Deep Work: Rules for Focused Success in a Distracted World` - Undoubtedly worthwhile.
- `The Psychology of Money` - Not a great book, attractive title but doesn't teach you much besides repeating the fact that markets are volatile.
- `Peak: Secrets from the New Science of Expertise` - Another fantastic Book recommended by my [great friend](https://twitter.com/AnimeshBulusu).

Finished some pending home improvement works for my new minimalistic home which I have purchased in late 2021. Mutation of bills like property tax, electricity bill is completed. Right now it's in livable condition and has got all the basic amenities and eligible for let out. But if I have to move in there, basic infra needs to be setup, and some more home improvement is required.

At workplace, my work has been recognized and got rewarded with a promotion. Also, I have gone through some formal process to get empaneled as an Interviewer. These things are good, but it doesn't really affect me much. What brings joy is this: I was working on couple of user stories, which demands to add few new features, but I see the major problem of spaghetti code in the codebase and refactored a good chunk of code that's been written by devs for over a period of 2 years which belongs to an epic(Agile Terminology). Honestly even the developer who's written couldn't read it quickly if asked to revisit and explain. I took some of my extra personal time and made it clean and readable which gives me satisfaction, and am sure fellow developers in my team would thank me later for making it easy to read by removing unnecessary complexity.

Besides work, I have finished an ad hoc personal project that does data sync between two data stores say MsSQL to MySQL. And, it's a scheduled recurring job that runs multiple times in a given day. At the outset it seems simple and one line requirement, but it's not that simple that much I can say. I have used Java(11), JDBC, Dev containers, in dev environment. Deployment stack consists of GoDaddy VPS having Alma Linux, 2 Core CPU, 512 MB memory, definitely not a powerful machine. Leveraged Linux `systemd` to make the java process run as daemon and scheduled it using timers.
