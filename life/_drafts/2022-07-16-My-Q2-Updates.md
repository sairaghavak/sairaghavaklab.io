---
# This is called Front matter
layout: post
title:  "2022-Q2 Updates"
date:   2022-07-16 08:37 IST
author: SaiRaghava K
categories: life
published: false
# Unpublihsed and moved to drafts folder
---

It's a good quarter, made some decent progress at work and in life, bought a brand-new car, read a book, and trying to improve on my skills, dedicated some time for relaxation and chill outs to prevent burnouts.

Read a book [Attitude is everything](https://www.goodreads.com/review/show/4686254614). Here is one of my favorite excerpt from this book.
  > I don’t claim to be a “know-it-all” on these subjects. Far from it. I consider myself a “work in progress” and I continue to learn every day.

Customizing this blog with my preferences on per need basis. Honing on my decisiveness, developing fisdom as a part of it trying to learn and understand securities, markets, indices, observing the trends in my geography at least. Did my research, homework to come up with my choice of make, model, and variant of the car. It's an experience going through the whole process of purchasing a car.

Coming to work, it's kinda getting monotonous at times, but still optimistic and trying to make my work enjoyable and productive. Besides that, worked on some of my personal projects created some dev containers for python environment, and load testing tools like [Locust](https://locust.io/), [wrk2](https://github.com/giltene/wrk2)

Changed my opinions, fallacies towards some people I knew from a very long time, realized that I misunderstood them. Developing immunity to negativity and criticism.

Couldn't agree more with this quote by [Sadhguru](https://en.wikipedia.org/wiki/Sadhguru)

> If you really wanted to be surrounded by the ideal people, go to heaven
