---
# This is called Front matter
layout: post
title:  "Exception Handling in Java - (In Progress)"
date:   2023-08-21 23:29 IST
author: SaiRaghava K
categories: 1-in-progress
comments: false
---

[![Exception Hierarchy](/assets/images/posts/exception-handling/exception-hierarchy.png)](/assets/images/posts/exception-handling/exception-hierarchy.png)

- **Topics pertinent to Exception Handling**
  - `throws`
  - `try`
  - `catch`
  - `throw`
  - `finally`
  - Try with resources
    - `java.lang.AutoCloseable`
  - Try with Multi-catch
  - Suppressed Exceptions
  - Log and re-throw or handle and declare
  - Swallowing exceptions - Antipattern
  - Antipatterns in exception handling

- **Some pairs of (Exceptions, Errors) from `java.lang` package**
  - `IllegalAccessException` / `IllegalAccessError`
  - `InstantiationException` / `InstantiationError`
  - `ClassNotFoundException` / `NoClassDefFoundError`
  - `NoSuchFieldException` / `NoSuchFieldError`
  - `NoSuchMethodException` / `NoSuchMethodError`

- [java.lang.IllegalStateException vs java.lang.IllegalAccessException](https://stackoverflow.com/a/22125139/792580)

**References:**

1. [Exception Handling in Java](https://en.wikipedia.org/wiki/Exception_handling)
2. [Exception Handling](https://docs.oracle.com/javase/tutorial/essential/exceptions/index.html)


---

PS: Will add little more details. Discussion/Comments will be enabled for this post after it's updated with details.