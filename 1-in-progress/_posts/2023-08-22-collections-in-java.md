---
# This is called Front matter
layout: post
title:  "Collections Framework in Java - (In Progress)"
date:   2023-08-22 11:31 IST
author: SaiRaghava K
categories: 1-in-progress
comments: false
---

- Excerpt from [Wikipedia](https://en.wikipedia.org/wiki/Data_structure)
  > - Abstract Data Type(ADT) - Serves the logical form of the data type - Conceptual idea, semantics, operations - user view
  > - Few of the common ADT's are
      - List/Sequence
      - Set
      - Map/Associative Array/Dictionary
      - Stack - LIFO - Has top of the stack - operations - push(), peek(), pop()
      - Queue - FIFO - Has head and tail - operations - offer(), peek(), poll()
      - Deque
      - PriorityQueue
      - Graph
      - Tree
  >
  > ---
  > - Data structure - Implements the physical form of the data type - talks about internal implementation of ADT - programmers view
      - **Array** - Contiguous memory allocation of elements
      - **LinkedList** - Each node is an object that is stored at memory. All nodes are scattered across memory but, all are linked with references.
      - **record/struct**
      - **HashTable** - Read my post: [Overview of HashTable implementation](https://sairaghavak.gitlab.io/java/2023/02/13/hashtable.html)
      - **Heap/Binary Heap** - Specialized tree-based data structure satisfying the heap property(Min-Heap property or Max-Heap property)
        - Often used to implement PriorityQueues for Min-Heap, Max-Heap
        - [Read Binary Heap](https://en.wikipedia.org/wiki/Binary_heap)
        - Worst case time complexity of operations
          - insert - O(log n)
          - Find-min - O(1)
          - Delete-min - O(log n)
- > Think of an ADT as an interface and data structure as an interface(ADT) implementation

List, Queue, and Set

[![Collections API(List, Queue, Set)](/assets/images/posts/collections-framework/imgs/collections-api-in-java.png)](/assets/images/posts/collections-framework/imgs/collections-api-in-java.png)

---

Map

[![Collections API(Map)](/assets/images/posts/collections-framework/imgs/collections-map-api-in-java.png)](/assets/images/posts/collections-framework/imgs/collections-map-api-in-java.png)

---

Summary of frequently used Collections

[![Summary of Collections API](/assets/images/posts/collections-framework/imgs/summary-of-collections.png)](/assets/images/posts/collections-framework/imgs/summary-of-collections.png)

----

**[Summary of Queue Methods](https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html)**

| | | |
|---| ---| ---|
|| Throws Exception <br> Fail fast Operation | Return special value often (true/false) <br> Fail safe Operation|
|Insert| add(e) |offer(e) |
|Remove| remove() | poll() |
|Examine| element() | peek() |

## Priority Queue

- [Priority Queue is an ADT that is implemented by Heap/Binary-Heap Data Structure.](https://stackoverflow.com/a/18993313/792580)
- By default, it stores the data as Min-Heap.
  - Note: The Min-Heap doesn't mean the underlying storage array is sorted in natural ordered.
  - Given an array, how a Min-Heap and Max-Heap data structure is created is shown below theoritically.
    [![min-max-heap-eg-1.png](/assets/images/posts/collections-framework/imgs/min-max-heap-eg-1.png)](/assets/images/posts/collections-framework/imgs/min-max-heap-eg-1.png)
    [![min-max-heap-eg-2.png](/assets/images/posts/collections-framework/imgs/min-max-heap-eg-2.png)](/assets/images/posts/collections-framework/imgs/min-max-heap-eg-2.png)

- Does not permit null elements because it expects the elements to implement `Comparable` or `Comparator` so that they are compared and placed in Binary heap structure.
- [Youtube video: Heap as Tree to Heap as Array](https://www.youtube.com/watch?v=fJORlbOGm9Y)

---

- **Topics pertinent to Collections Framework in Java**
  - `java.lang.Comparable` vs `java.util.Comparator`
    - `public int compareTo(T o);` vs `int compare(T o1, T o2);`
  - equals() and hashCode() contract
  - `java.util.Collections` static utility class
  - `java.util.ConcurrentModificationException` use-cases
  - `java.lang.UnsupportedOperationExpcetion` use-cases
  - External iteration(iterator and classic for loop, while loop, imperative way) vs internal iteration(for-each, declarative)
  - `java.util.Iterator` vs `java.util.ListIterator`
  - Generics in Java - Purpose, Type erasure
  - Capacity vs Size in `java.util.ArrayList`
  - Failfast vs Failsafe Collections
  - Synchronized vs Concurrent collections
  - `java.util.Vector` vs `java.util.ArrayList`
  - `java.util.concurrent.CopyOnWriteArrayList`, `java.util.concurrent.ConcurrentHashMap` - More reads, less/occasional writes
  - `java.util.Collections.synchronizedList(List<Object> list)`, `java.util.Collections.synchronizedMap(Map<K,V> m)` VS `java.util.concurrent.CopyOnWriteArrayList`, `java.util.concurrent.ConcurrentHashMap`
  - `java.util.Hashtable` vs `java.util.HashMap`
  - `java.util.Stack`(LIFO, Synchronized) vs `java.util.Deque`(LIFO + FIFO)
  - `java.util.PriorityQueue`
  - `java.util.concurrent.BlockingQueue`
    - `java.util.concurrent.PriorityBlockingQueue`
    - `java.util.concurrent.SynchronousQueue`
  - `java.util.Collections.emptyList()` vs `java.util.Collections.emptyMap()` vs `java.util.Collections.emptySet()`

---

PS: Will add little more details. Discussion/Comments will be enabled for this post after it's updated with details.
