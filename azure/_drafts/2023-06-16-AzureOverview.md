---
# This is called Front matter
layout: post
title:  "Azure Overview"
date:   2023-06-16 13:25 IST
author: SaiRaghava K
categories: azure
---


## Azure overview

- Login to `portal.azure.com`
  - **Directories:** under accounts, select your directory. This is applicable when you have multiple org accounts.
    - Directory will have domain name and directory-id, this is the tenant-id
    - **Subscriptions:** Then, select your subscription - Resource Type: Subscription
      - Each subscription will have SubscriptionId, SubsriptionName, DirectoryName, CurrentBillingPeriod, Currency
      - **Resource Groups** - Groups Azure resources together to a Deployment location like UK-SOUTH etc
        - Each resource group will have resources with resource types as
          - SQL Server
          - SQL Database
          - Storage Account: <https://learn.microsoft.com/en-us/azure/vs-azure-tools-storage-manage-with-storage-explorer?tabs=windows>
          - Virtual Network
          - Smart Detector alert rule
          - Shared Dashboard
          - Public IP Address
          - Private Endpoint
          - Private DNS zone
          - Network Security Group
          - Network Interface
          - Managed Identity
          - Key Vault
          - Function App: <https://learn.microsoft.com/en-us/azure/azure-functions/>
          - DDoS protection plan
          - Container Registry
          - Bastion
          - Application Insights
          - App Service Plan
          - App Service
          - App Configuration

- When developing azure functions, the application logs can be seen from
  - The path to see logs
    - > home -> &lt;subscription&gt; | Resource groups -> &lt;your-resource-group&gt; -> &lt;your-function-app&gt; | Log Stream

### References

1. [Azure](https://learn.microsoft.com/en-us/azure/?product=popular)
