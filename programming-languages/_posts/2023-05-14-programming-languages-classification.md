---
# This is called Front matter
layout: post
title:  "Programming Languages: Classifications and Generations"
date:   2023-05-14 09:00 IST
author: SaiRaghava K
categories: programming-languages
---

CPU can only understand 0's and 1's i.e., machine language. To run/execute tasks on a machine, we use programming languages as tools to feed CPU with the language that it understands.

There are 2 categories of programming languages.

1. Low-Level
2. High-Level

[![Programming Languages Classification](/assets/images/posts/programming-languages/pl.png)](/assets/images/posts/programming-languages/pl.png)

### Low-Level Programming languages(LLPL)

- The word low in this context means the closeness with the CPU.
- Low level languages are easy to read and understand for a CPU, but hard to read and program for a human.
- [**Machine language - First Generation Language**](https://en.wikipedia.org/wiki/First-generation_programming_language)
  - It is complex to program in machine language as it mostly has 0's and 1's in the source code file.
  - It's the language that a CPU can understand, when we program in machine language, we don't need a translator to let the CPU understand the instructions.
- [**Assembly Language - Second Generation Language**](https://en.wikipedia.org/wiki/Second-generation_programming_language)
  - Less complex compared to machine language and little abstract which means it's not so close to CPU and comes with vocabulary like `ADD`, `SUB`, `MUL` to perform basic math operations and many other mnemonics.
  - An Assembler is a program that converts the assembly code into native executable to run the program. Refer above image.
- **Note:** Both the assembly language and machine language are specific to CPU/processor family. Therefore, assembly/machine language code written for one processor may not work on another processor with different architecture or instruction set.

### High-Level Programming languages(HLPL)

- The word high in this context means it's highly abstract and not close to the language that CPU understands.
- HLPLs are easy to read and understand for a human just like English language. But, CPU cannot understand such languages. So, a language translator is required between HLPL and CPU, the translator could be a compiler or interpreter.
- Some HLPLs after code compilation compiles code to machine code i.e., native executable file.
- Other HLPLs follow a different approach that is the first step could be a compilation that compiles source code to an intermediate platform independent format like Bytecode in Java followed by interpretation that translates byte code instructions to machine code and fed to CPU on the fly without generating a machine code executable files before execution.
- [Third Generation Language(3GL)](https://en.wikipedia.org/wiki/Third-generation_programming_language) is almost close to English language meaning easier to read, more declarative, we just have to specify what to do, not how to do. Examples of 3GL are C, C++, Java, C#, Python, etc.
- [Fourth Generation Language(4GL)](https://en.wikipedia.org/wiki/Fourth-generation_programming_language) is just like English language which are much easier to read and write/program compared to 3GL. Example: SQL, R etc

In summary, programming languages have evolved a lot from the days of writing complex machine language, assembly language to easy to read and write declarative programming languages like SQL, Java, R, Python etc. Today, with modern programming languages we can create and run programs/software quickly with ease and elegance.

**References:**

1. [1GL](https://en.wikipedia.org/wiki/First-generation_programming_language)
2. [2GL](https://en.wikipedia.org/wiki/Second-generation_programming_language)
3. [3GL](https://en.wikipedia.org/wiki/Third-generation_programming_language)
4. [4GL](https://en.wikipedia.org/wiki/Fourth-generation_programming_language)
