---
# This is called Front matter
layout: post
title:  "Fav Music Tracks"
date: 2023-12-22 20:50 IST
author: SaiRaghava K
categories: non-tech-misc
comments: false
---


- Although I am not sure of the exact year, sometime between 2010-2012, while surfing the internet I discovered <https://chillstep.info> and it's one of the best online music stations. This page has some of my favorite filtered tracks from it and other personal playlists scattered across the different music streaming platforms like Spotify, SoundCloud.

---

- In the raw links below the suffix has `#t=0%3A00` that translates to `#t=0:01`, which means start playing the track from that time.

  - |URL Escape Code | Character|
    |---|---|
    |`%3A`| `:`|

---

- My fav tracks (In no particular order) and the raw links - Not an exhaustive list.

  |Track Title | Raw Link |
  |---|---|
  |Allan Wats|<https://soundcloud.com/conquestv/sets/allan-wats#t=0%3A01>|
  |Ain't No Love|<https://soundcloud.com/blackpumasofficial/aint-no-love-in-the-heart-of#tt=0%3A01> |
  |I Apologize | <https://soundcloud.com/five-finger-death-punch-official/i-apologize#t=0%3A01> |
  |I Think I'm Moving To Cali | <https://soundcloud.com/benriley/i-think-im-moving-to-cali#t=0%3A01> |
  |Forget me now | <https://soundcloud.com/cjayt/ngage-forget-me-now-chillstep#t=0%3A01> |
  |Suppressant | <https://soundcloud.com/yhproductions/yh-suppressant-ft-laura-brehm#t=0%3A01> |
  |Marina And The Diamonds - Lies (Zeds Dead Remix) |<https://soundcloud.com/ianlanza/marina-and-the-diamonds-lies-zeds-dead-remix#t=0%3A01>|
  |Elusive Dream| <https://soundcloud.com/politrix-2/elusive-dream#t=0%3A01>|
  |Inofaith-Nocturen| <https://soundcloud.com/madmel-madmel/inofaith-nocturne#t=0%3A01> |
  |Coldplay - Paradise (Ianborg Remix)| <https://soundcloud.com/jonanza/coldplay-paradise-ianborg#t=0%3A01> |
  |my way (cutz by huckspin)|<https://soundcloud.com/mr-ebs/mr-ebs-my-way-cutz-by-huckspin#t=0%3A01> |
  |Caspa - For The Kids | <https://soundcloud.com/filth-frenzy/caspa-for-the-kids#t=0%3A01> |
  |Jakwob ft. Mr Hudson - The Prize | <https://soundcloud.com/jakwob/10-jakwob-the-prize-ft-mr#t=0%3A01> |
  |dabin-koda-the-take-down-1 | <https://soundcloud.com/dabinlee/dabin-koda-the-take-down-1#t=0%3A01> |

**References:**

1. [HTML URl Escape Codes and Character Representations](https://docs.microfocus.com/OMi/10.62/Content/OMi/ExtGuide/ExtApps/URL_encoding.htm)
2. [ChillStep.info](https://chillstep.info/)
