---
# This is called Front matter
layout: privacy-policy
comments: false
permalink: /privacy-policy/
---


### Privacy Policy

- Cookies & Third-Party Services
    - Some third-party services (e.g., analytics, embedded content) may use cookies.
    - You can manage cookies through your browser settings.

- Analytics Tracking
    - This blog uses [**Google Analytics**](https://marketingplatform.google.com/about/analytics/) and [**Microsoft Clarity**](https://clarity.microsoft.com/) to track anonymous visitor data (e.g., page views, device types, and general location).
    - You can disable tracking by adjusting your browser settings.

- Comments
    - Uses [**Giscus**](https://giscus.app/) for comments, which requires a GitHub account.
    - Please read [**Giscus privacy policy**](https://github.com/giscus/giscus/blob/main/PRIVACY-POLICY.md)

- Data Sharing
    - This blog directly doesn't collect, use or store any personal data.

- Contact
    - If you have any questions, feel free to reach out at `sairaghavak@outlook.com`

---

<sub>This privacy policy is strictly applicable to this website but not applicable to internet links/websites that you click through this site. You should read the external link privacy policy.</sub>

<sub>By using this blog, you agree to this policy</sub>