---
layout: page
title: Artifacts
permalink: /artifacts/
---

## Docker Images

- `sairaghavak/base-dev-toolkit` - [https://hub.docker.com/r/sairaghavak/base-dev-toolkit](https://hub.docker.com/r/sairaghavak/base-dev-toolkit)

## Maven archetypes(Maven project templates)

- `JAXRS Client Archetype` - [https://mvnrepository.com/artifact/com.sairaghava/jaxrs-client-archetype](https://mvnrepository.com/artifact/com.sairaghava/jaxrs-client-archetype)
- `JavaSE Standalone App` - [https://mvnrepository.com/artifact/com.sairaghava/javase-standalone-app](https://mvnrepository.com/artifact/com.sairaghava/javase-standalone-app)
- `Jakarta EE Webapp` - [https://mvnrepository.com/artifact/com.sairaghava/jakarta-ee-webapp](https://mvnrepository.com/artifact/com.sairaghava/jakarta-ee-webapp)
