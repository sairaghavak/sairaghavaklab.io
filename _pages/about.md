---
layout: page
title: About
permalink: /about/
---



<img src="../images/sairaghavak-2024.jpg" alt="sairaghavak" class="about-style">

I am SaiRaghava (సాయిరాఘవ), working in IT since **2010**, and I am passionate about Software Engineering, Data Science.

I have created this blog to organize my learnings and thoughts on software.

<!-- Developed this blog using [Jekyll](https://jekyllrb.com/) a ruby based static site generator which is hosted on [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/). -->

<!-- My work day is incomplete without reading Stack Overflow threads. I participate in Q&A on Stack Overflow, by participate I mean, ask, answer, vote for, or comment on questions, and answers.<br>
Here is my Stack Overflow Flair ![](https://stackoverflow.com/users/flair/792580.png)
 -->
<!-- **Favorite Editor:** [VS Code](https://code.visualstudio.com/)<br/> -->

<!-- **Reading:** I read non-fiction: Psychology and self-help. Here is what I am reading now --->
<!--  [GoodReads](https://www.goodreads.com/user/show/20285137-sairaghava-k)<br/> -->
<!-- **Contact:** sairaghavak@outlook.com -->

Email: <sairaghavak@outlook.com>

<br>

<hr>

### Now

- Figuring out effective ways to improve dev productivity
- Restarted learning Python essentials for Data Science
- Conscious breathing, watching significant world affairs, observing, and thinking
- Reading occasionally these days - Offline reading - Took a break from Kindle

<hr>

### Other than Software

- Minimalist. Student for life.
- I have varied interests. A few of them include Psychology, Philosophy(Can listen to Alan Watts for hours). And <b>MORE...</b> <br>
- I pursue various physical activities:
  - One of them is Table-Tennis(I can play - Not a beginner, love to play backhand shots and chopping)
- Favorite quote for life:
  > <b><u>In the end, it all means nothing.</u></b>
- **<u>My current favorites</u>**
  - **Music:** This Masterpiece is worth playing in loop: [Intro - The XX](https://www.youtube.com/watch?v=xMV6l2y67rk)
  - **Songs with deep and layered lyrics:**
    - [Five Finger Death Punch - I Apologize](https://www.youtube.com/watch?v=OzvasAJIHb4)
    - [Three Days Grace - So Called Life](https://www.youtube.com/watch?v=7ViIny2YZH0)
  - **Movie**
    - **[Mission Impossible - Dead Reckoning Part One](https://www.youtube.com/watch?v=avz06PDqDbM)**
    - **[Animal](https://en.wikipedia.org/wiki/Animal_(2023_Indian_film))**
- To connect with me personally. Email: <ksairaghava1@gmail.com>

---

<!-- GoodReads reading updates -->
 <div>
  <br>
  <p> Occupied with other priorities, I will resume the reading activity. Here is my last reading update.</p>

  <style>
    #customize-list{
      float:left;
      margin-left:20px;
      border-bottom: 6px solid #EBEBEB;
    }
    #gr_updates_widget {
        border-radius: 2px;
        background-color: antiquewhite;
        /*border:solid #683205 5px;*/
        -webkit-box-shadow: 0px 0px 4px 1px #595959,
        inset 0px 0px 0px 1px #7D730B;
        -moz-box-shadow: 0px 0px 4px 1px #595959,
        inset 0px 0px 0px 1px #7D730B;
        box-shadow: 5px 0px 4px 1px #595959,
        inset 0px 0px 0px 1px #7D730B;
        padding:15px 0 0px 15px;
        width:350px;
        height:450px;
    }
    #gr_updates_widget p{
      padding:0px;
      margin:0;
      font-size:14px;
    }
    #gr_footer{
      margin-top:10px;
      margin-right: 15px;
    }
    #gr_footer hr {
      border: 2px solid black;
      border-radius: 2px;
      margin-right: 1px;
    }
    #gr_footer img{
      width:100px;
      float: left;
    }
    #gr_updates_widget img{
      border-style:none;
    }
  </style>

  <br>
  
  <div id="gr_updates_widget">
    <div id="gr_footer">
      <a href="https://www.goodreads.com/"><img alt="Goodreads: Book reviews, recommendations, and discussion" rel="nofollow" src="https://s.gr-assets.com/images/layout/goodreads_logo_140.png" /></a>
      <br>
      <hr>
    </div>
    <iframe sandbox id="the_iframe" src="https://goodreads.com/widgets/user_update_widget?user=20285137&num_updates=1&height=200&width=350" height="390" width="350" frameborder="0">
    </iframe>
  </div>
 
</div>
<!-- End of GoodReads reading updates -->

<hr>

<sub style="font-style: italic;font-size: x-medium;">
**Disclaimer:** Views expressed here are **<i>strictly</i>** my personal views and do not represent my employer views or any other entities. Any third party tools or technology names mentioned here are not endorsements.
</sub>
