---
layout: page
title: Categories
permalink: /categories/
---
<!-- Start the counter from 1 -->
<!-- For debugging use this {{post.title}} and {{counter}}>
<!-- <p>{{post.title}} {{counter}}</p> -->

{% assign counter = 1 %}
    {% for post in site.posts %}
      {% assign counter = counter | plus: 1 %}
    {% endfor %}
<p>Total Posts: {{ counter }}</p>

{%- include categories.html -%}
