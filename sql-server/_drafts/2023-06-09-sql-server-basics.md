---
# This is called Front matter
layout: post
title:  "MsSQL Server SQL commands"
date: 2023-06-09 04:48 IST
author: SaiRaghava K
categories: sql-server
---

- Normally, in SQL use `desc <table-name>`. To see the table info in sql server we use.
  - > exec sp_help 'table-name'

- In postgresql or other sql compliant databases we use limit and offset to get records from a specific row number and limit them. In sql server it's different syntax as shown below
  - > SELECT col1, col2
    >
    > FROM table
    >
    > ORDER BY col1
    >
    > OFFSET 10 ROWS FETCH NEXT 10 ROWS ONLY;
    >
    > -- start from row 11 and get upto 20(inclusive)

- Mapping data types from SQL server to Java
  - Read: [https://learn.microsoft.com/en-us/sql/language-extensions/how-to/java-to-sql-data-types?view=sql-server-ver16](https://learn.microsoft.com/en-us/sql/language-extensions/how-to/java-to-sql-data-types?view=sql-server-ver16)

- Declaring variables and substitute them in queries, so that in long queries you don't have to search for dynamic values and replace them.

  - ```sql
    DECLARE @id varchar(100);
    SET @id = '111'
    SELECT * 
    From auth_app.USER
    WHERE id = @id
    ```

**References:**

1. [Java and SQL Server Supported Data types](https://learn.microsoft.com/en-us/sql/language-extensions/how-to/java-to-sql-data-types?view=sql-server-ver16)
