---
# This is called Front matter
layout: post
title:  "GoF Design Patterns Overview"
date:   2023-04-17 01:00 IST
author: SaiRaghava K
categories: gof-design-patterns
---

23 GoF Design patterns are categorized into 3 categories as shown below.

[![GoF 23 Design Patterns](/assets/images/posts/design-patterns/dp-overview.png)](/assets/images/posts/design-patterns/dp-overview.png)

Each category has a specific goal

1. Creational: Goal is to provide a mechanism to create objects in way that promotes flexibility and reusability.
2. Structural: Deals with object composition to form larger structures, while keeping the system as simple and efficient as possible
3. Behavioral: Talks about how objects communication with each other to achieve specific goals.

Refer categories section to know more about specific patterns.
