---
# This is called Front matter
layout: post
title:  "Flyweight Pattern"
date:   2023-05-13 12:09 IST
author: SaiRaghava K
categories: gof-design-patterns
---

**References:**

1. [Flyweight Pattern](https://en.wikipedia.org/wiki/Flyweight_pattern)
