---
# This is called Front matter
layout: post
title:  "GraphQL Overview"
date:   2023-03-02 00:25 IST
author: SaiRaghava K
categories: graphql
---

## How Graphql differs from REST

- Traditionally, we call multiple APIs to get the data we want. But, there is more to it, we read from multiple api responses, parse response, filter, and read the required data.
- On the contrary, we are delegating this parsing, filtering, aggregating data to the API itself, and we will make one http POST API call to the graphql endpoint. In the request body we ask for what we want we'll exactly what we wanted nothing more or less.

## GraphQL

- Here is the specification of [Graphql](http://spec.graphql.org/draft/)
- Every Graphql request is a http `POST` request
  - The body of the post request contains
    - `query` to get state of the application/service
    - `mutation` changes the state of the application/service

## Spring with GraphQL

asdfs

**References:**

1. [GraphQL offical site](https://graphql.org/)
2. [Github Graphql Guides](https://docs.github.com/en/graphql/guides/introduction-to-graphql)
3. [Github Graphql Explorer](https://docs.github.com/en/graphql/overview/explorer)
4. [Microsoft Learning](https://learn.microsoft.com/en-us/shows/graphql/intro-to-graphql-part-1)
5. [Spring with GraphQL](https://www.linkedin.com/learning/spring-with-graphql/building-a-graphql-api-with-spring-boot?autoplay=true&u=2130370)
6. [Linkedin learning course by Emmanuel Henri](https://www.linkedin.com/learning/migrating-from-rest-to-graphql/replace-rest-with-graphql?autoplay=true&u=2130370)
