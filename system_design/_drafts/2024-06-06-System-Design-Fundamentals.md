---
# This is called Front matter
layout: post
title:  "Sytem Design Fundamentals"
date: 2024-06-06 13:46 IST
author: SaiRaghava K
categories: system-design
---


- Ideal response time for an API is: 

- System design process: Usually it will be 45 mins interview
  1. Understand the requirements
      - **Functional Requirements**
        - Example FRs for twitter
          - user should be able to create and login to account
          - Create, edit and delete tweets
          - Follow other users
          - View timeline of tweets from the people the user is following
          - Like, reply & retweeet
          - Search for tweets
      - **Non-Functional Requirements**
        - Scaling: Should support high volumes of tweets created. High throughput.
        - Available: 99.9% of uptime
        - Security: Authentication & Authorization, Encryption, Rate Limiting, and input validation.
        - Performance: Low latency or in other words faster response times
  2. High Level Design(Components)
     - Start with Client(Mobile, Desktop, Web) -> Load Balancer -> API Gateway
     - API Services or Servers
        - Your backend services can be deployed to
          - > EC2 or ECS(Elastic Container Services)
     - List Down all the APIs
  3. Drill Down or Low Level design
     - List down the responsibilities of each component
     - Schema
       - 


## Common Patterns

- Usually the user journey and initial components are same in 99% of the systems like
  - > Client(Mobile, Desktop, Web) -> Load Balancer(NGINX, AWS ELB) -> API Gateway

## Load Balancer Algorithms

- **Round Robin:** Distributes requests sequentially across a group of servers
  - `NGNIX` load balancer by default uses the Round robin approach.
  - Every new request and sent to the next instance
    - Example: Assume there are 3 instances of the `ServiceA`
      - Req1 -> instance1
      - Req2 -> instance2
      - Req3 -> instance3
      - Req4 -> instance1
- **Least Connections**
  - Directs traffic to the server with fewest active connections i.e., the server that is less busy or having less load/work
- **IP Hash**
  - The server selection is based on the hash of the client's IP address
  - Think of it like HashTable where in the key determines the bucket
  - Ensures that a client will consistently connect to the same server, which can be useful for session persistence
- **Least Time**
  - Directs traffic to the server with lowest average response time and least number of active connections
  - Useful for optimizing response times

## Keywords

- Functional Requirements
- NonFunctional Requirements
- TPS/RPS/QPS - Transactions pers second or Requests per second or Queries per second
- Given Avg users per day as 10M users. Find the RPS
  - 10,00000/24/3600 = 11.5 req/sec
- If there 10M messages send per day. How many messages are sent per second.
  - 100,00000/24/3600 = 115.740740741 req/sec
  - Peak is 

- Popular System Design questions
  - Design Spotify. Upload, Stream. Or any streaming services like youtube or Netflix.
    - Reference video: https://www.youtube.com/watch?v=_K-eupuDVEc
  - Design a file sharing system like One Drive, GDrive, Dropbox, or iCloud
    - Reference video: https://www.youtube.com/watch?v=4_qu1F9BXow
  - Design Twitter:
    - Reference video: 
  - Design Whatsapp or a chat application
    - Reference video: https://www.youtube.com/watch?v=M6UZ7pVD-rQ
  - Design a caching system or a distributed cache
  - Design Facebook or a social network
  - 

- What is Ingress used for?
  - From ChatGPT
    - Ingress itself is not a load balancer, but it can work with a load balancer to manager incoming traffic to your Kubernetes cluster
    - In Kubernetes, an Ingress is a collection of rules that allow inbound connections to reach services within the cluster. It provides HTTP and HTTPS routing to services based on hostnames and paths, acting as a layer 7 (application layer) proxy.
    - While Ingress manages the traffic routing and rules, it typically relies on an external load balancer (like NGINX, HAProxy, or cloud provider load balancers) to distribute the traffic to the correct pods within the cluster based on those rules.
    - So, in a Kubernetes environment, Ingress and load balancing work together to manage and distribute incoming traffic, but Ingress itself is not a load balancer in the traditional sense.
- What is reverse Proxy pattern?
- Sticky Session: When do we use it, what is its purpose?
- Does API Gateway also does load balancing or do we need an explicit load balancer?
- How does a load balancer work? Loader balancer examples: HAProxy, NGINX?
- Typical responsibilities of API gateway?
  - API management
  - IP whitelisting, blacklisting
  - API versioning
  - Request routing
  - Security
  - Rate limiting
  - Analytics
- Do we have to scale API gateway?
  - Yes, it can be scaled horizontally in high traffic systems
- Always load balancer comes before API gateway
- Why and when do we need an explicit load balancer when API gateways like AWS API Gateway, KONG, APIGEE, and others comes with built-in load balancing features?
  - Separate load balancer is recommended in traffic volume systems
    - High Availability: Ensures that the traffic is always directed to a healthy API Gateway instance
    - Scalability: Allows scaling out API Gateway instances independently from backend services
  - When API Gateway alone is sufficient?
    - When your application is a moderate traffic app
      - When traffic volume is manageable by the API gateway's built-in load balancing
      - Simpler Architecture: For smaller systems where simplicity is preferred, relying solely on the API Gateway can reduce complexity.
    - 


**References:**

1. [System Design from Wikipedia](https://en.wikipedia.org/wiki/Systems_design)
