---
# This is called Front matter
layout: post
title:  "Containerized Self-Managed SonarQube Setup"
date:   2025-02-03 23:27 IST
author: SaiRaghava K
categories: run-books
---

This post will be focus on setting up self-managed [SonarQube(Server)](https://www.sonarsource.com/products/sonarqube/)

Here is my setup steps(includes issues and fixes)

Here is my `docker-compose.yml` file

```yml
# @author: sairaghava_k
services:
  sonarqube:
    image: sonarqube:community
    hostname: sonarqube
    container_name: sonarqube
    read_only: true
    depends_on:
      db:
        condition: service_healthy
    environment:
      SONAR_JDBC_URL: jdbc:postgresql://db:5432/sonar
      SONAR_JDBC_USERNAME: sonar
      SONAR_JDBC_PASSWORD: sonar
    volumes:
    - sonarqube_data:/opt/sonarqube/data
    - sonarqube_extensions:/opt/sonarqube/extensions
    - sonarqube_logs:/opt/sonarqube/logs
    - sonarqube_temp:/opt/sonarqube/temp
    ports:
    - "9000:9000"
  db:
    image: postgres:15
    healthcheck:
      test: [ "CMD-SHELL", "pg_isready -d $$POSTGRES_DB -U $$POSTGRES_USER"]
      interval: 10s
      timeout: 5s
      retries: 5
    hostname: postgresql # this is hostname within the docker network
    container_name: postgresql
    #user: postgres
    environment:
      POSTGRES_USER: sonar
      POSTGRES_PASSWORD: sonar
      POSTGRES_DB: sonar
    volumes:
    - postgresql:/var/lib/postgresql
    - postgresql_data:/var/lib/postgresql/data
    ports: # This ports was missing so while I was trying to connect from local it couldn't connect as the port is not exposed outside the container
      - "5432:5432"

volumes: # docker-volumes
  sonarqube_data:
  sonarqube_temp:
  sonarqube_extensions:
  sonarqube_logs:
  postgresql:
  postgresql_data:


networks:
  default:
    name: srk-local-sonarqube-server-network
```

- When I do `docker network ls` Here is my network details

  |---|---|---|---|
  |05035d498616|srk-local-sonarqube-server-network|bridge|local|

- Tried to do `docker-compose up -d`
  - **Issue:** I faced this issue of
    > docker-credential-secretservice: error while loading shared libraries: libsecret-1.so.0: cannot open shared object file: No such file or directory
  - Understood that the `docker-credential-secretservice` requires the linux package: `libsecret-1-0`
  - **Fix:**
    > sudo apt-get install libsecret-1-0
  - **Verification**
    - From current CLI issued the command
      > docker-credential-secretservice version
      - Got response
        > docker-credential-secretservice (github.com/docker/docker-credential-helpers) v0.8.2
    - I was still getting the error
      - It says `error getting credentials`
      - Tried to do `docker login`
      - After that `docker-compose up -d` succeeded
       -I have verified my PAT's under my docker account in dockerhub(Web in the UP) and it has shown a new PAT token with {read,write, delete permissions}

- Details of my `docker-compose up -d` command
  [![docker-compose up -d](/assets/images/posts/run-books/sonar-qube/docker-compose-setup.png)](/assets/images/posts/sonar-qube/docker-compose-setup.png)
- sonar-qube service didn't start it's failing
  [![dokcer service startup failure](/assets/images//posts/run-books/sonar-qube/sonarqube-service-startup-failure.png)](/assets/images//posts/run-books/sonar-qube/sonarqube-service-startup-failure.png)
- To resolve this issue in linux
  - We need to increase the `vm.max_map_count` setting on your Linux system. You can do this by following these steps:
    - Run `sudo sysctl -w vm.max_map_count=262144`
    - To make this change permanent, add the following line to the `sysctl.conf` file:
      - `echo "vm.max_map_count=262144" | sudo tee -a /etc/sysctl.conf`
    - Apply the changes by running
      - `sudo sysctl -p`
    - This will increase the `vm.max_map_count` setting and resolve the bootstrap check failure for Elasticsearch.
    - Note: Here `262144` represents the new limit for the max number of memory map ares or process can have. his setting is crucial for applications like Elasticsearch that require a large number of memory mappings. Increasing this limit helps prevent errors related to insufficient virtual memory areas.
- Once all the services are up and running, try to hit the sonar-qube server UI at `localhost:9000`
  - username: admin
  - password: admin

---

## Clean up the docker-compose and recreate from scratch

- Commands to recreate the docker-compose stack
  - `docker-compose stop`
  - `docker-compose rm`
  - `docker-compose up -d`
- `docker-compose up -d` output
  - [![docker service started](/assets/images/posts/run-books/sonar-qube/1-udpated-services-started.png)](/assets/images/posts/run-books/sonar-qube/1-udpated-services-started.png)
- Services statuses
  - [![Services statuses](/assets/images/posts/run-books/sonar-qube/2-updated-service-status.png)](/assets/images/posts/run-books/sonar-qube/2-updated-service-status.png)
- postgresql service log
  - [![postgresql service log](/assets/images/posts/run-books/sonar-qube/3-updated-postrgresql-service-log.png)](/assets/images/posts/run-books/sonar-qube/3-updated-postrgresql-service-log.png)
- `docker logs -tail 10 sonarqube`
  - [![docker service started](/assets/images/posts/run-books/sonar-qube/4-updated-sonarqube-service-log.png)](/assets/images/posts/run-books/sonar-qube/4-updated-sonarqube-service-log.png)

- Read/Revisit to understand what how does the sonar-scanner work?
  - <https://docs.sonarsource.com/sonarqube-server/latest/analyzing-source-code/scanners/sonarscanner-for-gradle/#groovy-dsl>

---

## Attaching this server to gradle build tool

- Refer: <https://docs.sonarsource.com/sonarqube-server/latest/analyzing-source-code/scanners/sonarscanner-for-gradle/#groovy-dsl>
  - OR here is my sample config

    ```gradle
      // Groovy dsl
      sonar {
        properties {
            property "sonar.host.url", project.findProperty("sonar.host.url") ?: "default-project-url"
            property "sonar.projectKey", project.findProperty("sonar.projectKey") ?: "default-project-key"
            property "sonar.token", project.findProperty("sonar.token") ?: "default-token"
        }
      }
    ```

- These props are looked up from
  `~/.gradle/gradle.properties`
- Here are the relevant properties as entries

  ```properties
  #sairaghavak - Adding sonarqube local sonarqube server properties
  sonar.host.url=http://localhost:9000
  sonar.projectKey=<your-project-name-or-any-unique-identifier>
  sonar.token=squ_c55326f8bfcdd2b467dc2d53bad4563fae2094ac
  ```

- `gradle sonar` would scan your project and updates analysis data to sonarqube server. Once the gradle task execution is complete, verify the project sonar analysis report in sonar UI at `sonar.host.url`

- Here is the snap of the postgresql db that backs sonarqube server
  [![psql db of sonarqube server](/assets/images/posts/run-books/sonar-qube/5-sonarqube-server-postgresql-db.png)](/assets/images/posts/run-books/sonar-qube/5-sonarqube-server-postgresql-db.png)

---

## Generating a report from SonarQube server

- There are no free community plugins available for sonarQube server at the time of my setup.

**References:**

1. [Official Sonarqube docker-compose](https://github.com/SonarSource/docker-sonarqube/blob/master/example-compose-files/sq-with-postgres/docker-compose.yml)
2. [Official SonaqQube](https://docs.sonarsource.com/sonarqube-server/latest/)
3. [Sample gradle sonar setup project](https://github.com/SonarSource/sonar-scanning-examples/tree/master/sonar-scanner-gradle/gradle-basic)
