---
# This is called Front matter
layout: post
title:  "Design Principles: {DRY, YAGNI, KISS}"
date: 2024-11-05 17:17 IST
author: SaiRaghava K
categories: design-principles
---

### Brief

|Acronym|Expansion|Goal|
|---|---|---|
|DRY|Don't Repeat Yourself|Avoid duplication of code|
|YAGNI|You Aren't Gonna Need It|Don't add functionality until it's needed|
|KISS|Keep it short and Simple |Keep the code simple.|

---

### Details

- **DRY**
    - Talks about avoiding code duplication in Software Development.
    - Example:
    ```java
      // Example of Code Duplication
      public class EmployeeService {
        public void saveEmployee(Employee employee) {
            if (employee == null 
                || employee.name == null 
                || employee.name.isEmpty()) {
              throw new IllegalArgumentException("Employee Object is not valid");
            }
            // Save employee to database
        }

        public void updateEmployee(Employee employee) {
            if (employee == null 
                || employee.name == null 
                || employee.name.isEmpty()) {
              throw new IllegalArgumentException("Employee Object is not valid");
            }
            // Update employee in database
        }
      }
    ```
    - Fix
    ```java
      public class EmployeeService {
        public void saveEmployee(Employee employee) {
            validateEmployee(employee);
            // Save employee to database
        }
        public void updateEmployee(Employee employee) {
            validateEmployee(employee);
            // Update employee in database
        }
        private void validateEmployee(Employee employee) {
            if(employee == null 
                || employee.name == null 
                || employee.name.isEmpty()) {
              throw new IllegalArgumentException("Employee Object is not valid");
            }
        }
      }
    ```
  
---

- **YAGNI**
    - Goal is to avoid adding functionality until it's needed.
    - Example:
    - ```java
      public class EmployeeService {
        public void saveEmployee(Employee employee) {
            validateEmployee(employee);
            // This is not needed now, but may be needed in future
            publishEmployeeForAnalytics(employee); 
            // Save employee to database
        }
      }
      ```
    - Fix
      ```java
      public class EmployeeService {
        public void saveEmployee(Employee employee) {
            validateEmployee(employee);
            // Removed things that are not needed now
            // Save employee to database
        }
      }
        ```

---

- **KISS**
    - Keep it short and simple.
    - Goal is to keep the code clean and simple. Focuses on correctness, readability, and performance.
    - Correctness - Make it work first. Don't worry about performance at this step.
    - Readability - Make it readable.
    - Performant - Make it performant. Optimize the code for performance.
    - Example:
    - ```java
      public class StringUtils {
        public static String reverse(String str) {
            String reverse = "";
            for (int i = x.length() - 1; i >= 0; i--) {
                reverse += x.charAt(i);
            }
            return reverse;
        }
      }
        ```
    - Fix
      ```java
      public class StringUtils {
        public static String reverse(String str) {
            return new StringBuilder(str).reverse().toString();
        }
      }
      ```

**References**

1. [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself)
2. [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it)
3. [KISS](https://en.wikipedia.org/wiki/KISS_principle)
4. [YGNI vs KISS](https://stackoverflow.com/a/52804673/792580)