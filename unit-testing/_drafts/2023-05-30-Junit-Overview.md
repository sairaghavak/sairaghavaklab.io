---
# This is called Front matter
layout: post
title:  "Junit5 Overview"
date: 2023-05-31 10:38 IST
author: SaiRaghava K
categories: unit-testing
---

This post covers the following topics

> - How to install Junit5 in a project?
> - What are the dependencies to be added in your build config like pom.xml?
> - LifeCycle of Junit5
> - Commonly used annotations with examples
> - Limitations of Junit5

- Junit5 requires Java8 or higher at runtime.

**References:**

1. [Junit5](https://junit.org/junit5/docs/current/user-guide/)
