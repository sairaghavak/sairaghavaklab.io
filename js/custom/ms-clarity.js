// @sairaghava_k added Microsoft Clarity Analytics-
// Manual Installation procedure
// Installed on 23rd Jan 2023
(function (c, l, a, r, i, t, y) {
  c[a] =
    c[a] ||
    function () {
      (c[a].q = c[a].q || []).push(arguments);
    };
  t = l.createElement(r);
  t.async = 1;
  t.src = "https://www.clarity.ms/tag/" + i;
  y = l.getElementsByTagName(r)[0];
  y.parentNode.insertBefore(t, y);
})(window, document, "clarity", "script", "finpicgfwz"); // Note: finpicgfwz is the Ms-Clarity Unique project id
