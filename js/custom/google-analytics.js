// @sairaghava_k added Google Analytics
// Manual Installation procedure
// Installed on 23rd Jan 2023
// Google tag (gtag.js)
window.dataLayer = window.dataLayer || [];
function gtag() {
  dataLayer.push(arguments);
}
gtag("js", new Date());

gtag("config", "G-DM8YEV054L"); // G-DM8YEV054L  - this is your google tag, formally known as global site tag
