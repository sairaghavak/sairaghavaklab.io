/*-@author: sairaghava_k */

// My Customized js specific to this blog

var searchInputText = document.getElementById("search-input");
var searchResultsContainer = document.getElementById("results-container");
var actualPostOrPageBodyContent = document.getElementById("actual-content-div");

/*- Below is the code specific to blog search functionality that applies dynamic styles from js */
window.onload = resetDisplays();
searchInputText.addEventListener('input', function(e) {
  // Perform search logic here with e.target.value
  // pass the input search data to simpleJekyllSearch js exteral/third-party plugin
  SimpleJekyllSearch({
    searchInput: searchInputText,
    resultsContainer: searchResultsContainer,
    // Declartively instructing the SimpleJekyllSearch js plugin to look up search.json and understand how to pull data and do search

    // Refer: https://github.com/christian-fei/Simple-Jekyll-Search
    // This search.json tells which data to search like posts, pages and in posts, pages should it search content, description, title etc
    json: "/search.json",
  });
  resetDisplays();
});

/* searchInputText.addEventListener("keyup", (e) => {
  // keypress, keydown is not working, only keyup is working in my case
  console.log("In searchbox keyup event fired ");
  // pass the input search data to simpleJekyllSearch js exteral/third-party plugin
  SimpleJekyllSearch({
    searchInput: searchInputText,
    resultsContainer: searchResultsContainer,
    json: "/search.json",
  });
  resetDisplays();
}); */

function resetDisplays() {
  if (searchInputText.value !== "") {
    searchResultsContainer.className = "visible";
    actualPostOrPageBodyContent.className = "hidden";
  } else {
    searchResultsContainer.className = "hidden";
    actualPostOrPageBodyContent.className = "visible";
  }
}
